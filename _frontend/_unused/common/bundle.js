/*

 MyFonts Webfont Build ID 3296313, 2016-10-16T15:30:58-0400

 The fonts listed in this notice are subject to the End User License
 Agreement(s) entered into by the website owner. All other parties are
 explicitly restricted from using the Licensed Webfonts(s).

 You may obtain a valid license at the URLs below.

 Webfont: Brother-1816-Book by TipoType
 URL: http://www.myfonts.com/fonts/tipotype/brother-1816/book/
 Licensed pageviews: 2,010,000

 Webfont: Brother-1816-Medium by TipoType
 URL: http://www.myfonts.com/fonts/tipotype/brother-1816/medium/
 Licensed pageviews: 2,000,000

 Webfont: Brother-1816-Bold by TipoType
 URL: http://www.myfonts.com/fonts/tipotype/brother-1816/bold/
 Licensed pageviews: 2,000,000


 License: http://www.myfonts.com/viewlicense?type=web&buildid=3296313
 Webfonts copyright: Copyright (c) 2016 by Ignacio Corbo and Fernando Diaz. All rights reserved.

 ? 2016 MyFonts Inc
*/
var customPath = "/common/css/external/fonts";

var protocol=document.location.protocol;"https:"!=protocol&&(protocol="http:");var count=document.createElement("script");count.type="text/javascript";count.async=!0;count.src=protocol+"//hello.myfonts.net/count/324c39";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(count,s);var browserName,browserVersion,webfontType;if("undefined"==typeof woffEnabled){ var woffEnabled=!0; }var svgEnabled=0,woff2Enabled=1;
if("undefined"!=typeof customPath){ var path=customPath; }else{var scripts=document.getElementsByTagName("SCRIPT"),script=scripts[scripts.length-1].src;script.match("://")||"/"==script.charAt(0)||(script="./"+script);path=script.replace(/\\/g,"/").replace(/\/[^\/]*\/?$/,"")}
var wfpath=path+"/webfonts/",browsers=[{regex:"MSIE (\\d+\\.\\d+)",versionRegex:"new Number(RegExp.$1)",type:[{version:9,type:"woff"},{version:5,type:"eot"}]},{regex:"Trident/(\\d+\\.\\d+); (.+)?rv:(\\d+\\.\\d+)",versionRegex:"new Number(RegExp.$3)",type:[{version:11,type:"woff"}]},{regex:"Firefox[/s](\\d+\\.\\d+)",versionRegex:"new Number(RegExp.$1)",type:[{version:3.6,type:"woff"},{version:3.5,type:"ttf"}]},{regex:"Edge/(\\d+\\.\\d+)",versionRegex:"new Number(RegExp.$1)",type:[{version:12,type:"woff"}]},
{regex:"Chrome/(\\d+\\.\\d+)",versionRegex:"new Number(RegExp.$1)",type:[{version:36,type:"woff2"},{version:6,type:"woff"},{version:4,type:"ttf"}]},{regex:"Mozilla.*Android (\\d+\\.\\d+).*AppleWebKit.*Safari",versionRegex:"new Number(RegExp.$1)",type:[{version:4.1,type:"woff"},{version:3.1,type:"svg#wf"},{version:2.2,type:"ttf"}]},{regex:"Mozilla.*(iPhone|iPad).* OS (\\d+)_(\\d+).* AppleWebKit.*Safari",versionRegex:"new Number(RegExp.$2) + (new Number(RegExp.$3) / 10)",unhinted:!0,type:[{version:5,
type:"woff"},{version:4.2,type:"ttf"},{version:1,type:"svg#wf"}]},{regex:"Mozilla.*(iPhone|iPad|BlackBerry).*AppleWebKit.*Safari",versionRegex:"1.0",type:[{version:1,type:"svg#wf"}]},{regex:"Version/(\\d+\\.\\d+)(\\.\\d+)? Safari/(\\d+\\.\\d+)",versionRegex:"new Number(RegExp.$1)",type:[{version:5.1,type:"woff"},{version:3.1,type:"ttf"}]},{regex:"Opera/(\\d+\\.\\d+)(.+)Version/(\\d+\\.\\d+)(\\.\\d+)?",versionRegex:"new Number(RegExp.$3)",type:[{version:24,type:"woff2"},{version:11.1,type:"woff"},
{version:10.1,type:"ttf"}]}],browLen=browsers.length,suffix="",i=0;
a:for(;i<browLen;i++){var regex=new RegExp(browsers[i].regex);if(regex.test(navigator.userAgent)){browserVersion=eval(browsers[i].versionRegex);var typeLen=browsers[i].type.length;for(j=0;j<typeLen;j++){ if(browserVersion>=browsers[i].type[j].version&&(1==browsers[i].unhinted&&(suffix="_unhinted"),webfontType=browsers[i].type[j].type,"woff"!=webfontType||woffEnabled)&&("woff2"!=webfontType||woff2Enabled)&&("svg#wf"!=webfontType||svgEnabled)){ break a } }}else { webfontType="woff" }}
/(Macintosh|Android)/.test(navigator.userAgent)&&"svg#wf"!=webfontType&&(suffix="_unhinted");var head=document.getElementsByTagName("head")[0],stylesheet=document.createElement("style");stylesheet.setAttribute("type","text/css");head.appendChild(stylesheet);
for(var fonts=[{fontFamily:"Brother1816",fontWeight:"normal",fontStyle:"normal",url:wfpath+"324C39_0"+suffix+"_0."+webfontType},{fontFamily:"Brother1816",fontWeight:"bold",fontStyle:"normal",url:wfpath+"324C39_1"+suffix+"_0."+webfontType},{fontFamily:"Brother-1816-Bold",url:wfpath+"324C39_2"+suffix+"_0."+webfontType}],len=fonts.length,css="",i=0;i<len;i++){var format="svg#wf"==webfontType?'format("svg")':"ttf"==webfontType?'format("truetype")':"eot"==webfontType?"":'format("'+webfontType+'")',css=
css+("@font-face{font-family: "+fonts[i].fontFamily+";src:url("+fonts[i].url+")"+format+";");fonts[i].fontWeight&&(css+="font-weight: "+fonts[i].fontWeight+";");fonts[i].fontStyle&&(css+="font-style: "+fonts[i].fontStyle+";");css+="}"}stylesheet.styleSheet?stylesheet.styleSheet.cssText=css:stylesheet.innerHTML=css;
!function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e():"function"==typeof define&&define.amd?define(e):t.ES6Promise=e()}(this,function(){"use strict";function t(t){return"function"==typeof t||"object"==typeof t&&null!==t}function e(t){return"function"==typeof t}function n(t){I=t}function r(t){J=t}function o(){return function(){return process.nextTick(a)}}function i(){return function(){H(a)}}function s(){var t=0,e=new V(a),n=document.createTextNode("");return e.observe(n,{characterData:!0}),function(){n.data=t=++t%2}}function u(){var t=new MessageChannel;return t.port1.onmessage=a,function(){return t.port2.postMessage(0)}}function c(){var t=setTimeout;return function(){return t(a,1)}}function a(){for(var t=0;t<G;t+=2){var e=$[t],n=$[t+1];e(n),$[t]=void 0,$[t+1]=void 0}G=0}function f(){try{var t=require,e=t("vertx");return H=e.runOnLoop||e.runOnContext,i()}catch(n){return c()}}function l(t,e){var n=arguments,r=this,o=new this.constructor(p);void 0===o[et]&&k(o);var i=r._state;return i?!function(){var t=n[i-1];J(function(){return x(i,o,t,r._result)})}():E(r,o,t,e),o}function h(t){var e=this;if(t&&"object"==typeof t&&t.constructor===e){ return t; }var n=new e(p);return g(n,t),n}function p(){}function v(){return new TypeError("You cannot resolve a promise with itself")}function d(){return new TypeError("A promises callback cannot return that same promise.")}function _(t){try{return t.then}catch(e){return it.error=e,it}}function y(t,e,n,r){try{t.call(e,n,r)}catch(o){return o}}function m(t,e,n){J(function(t){var r=!1,o=y(n,e,function(n){r||(r=!0,e!==n?g(t,n):S(t,n))},function(e){r||(r=!0,j(t,e))},"Settle: "+(t._label||" unknown promise"));!r&&o&&(r=!0,j(t,o))},t)}function b(t,e){e._state===rt?S(t,e._result):e._state===ot?j(t,e._result):E(e,void 0,function(e){return g(t,e)},function(e){return j(t,e)})}function w(t,n,r){n.constructor===t.constructor&&r===l&&n.constructor.resolve===h?b(t,n):r===it?j(t,it.error):void 0===r?S(t,n):e(r)?m(t,n,r):S(t,n)}function g(e,n){e===n?j(e,v()):t(n)?w(e,n,_(n)):S(e,n)}function A(t){t._onerror&&t._onerror(t._result),T(t)}function S(t,e){t._state===nt&&(t._result=e,t._state=rt,0!==t._subscribers.length&&J(T,t))}function j(t,e){t._state===nt&&(t._state=ot,t._result=e,J(A,t))}function E(t,e,n,r){var o=t._subscribers,i=o.length;t._onerror=null,o[i]=e,o[i+rt]=n,o[i+ot]=r,0===i&&t._state&&J(T,t)}function T(t){var e=t._subscribers,n=t._state;if(0!==e.length){for(var r=void 0,o=void 0,i=t._result,s=0;s<e.length;s+=3){ r=e[s],o=e[s+n],r?x(n,r,o,i):o(i); }t._subscribers.length=0}}function M(){this.error=null}function P(t,e){try{return t(e)}catch(n){return st.error=n,st}}function x(t,n,r,o){var i=e(r),s=void 0,u=void 0,c=void 0,a=void 0;if(i){if(s=P(r,o),s===st?(a=!0,u=s.error,s=null):c=!0,n===s){ return void j(n,d()) }}else { s=o,c=!0; }n._state!==nt||(i&&c?g(n,s):a?j(n,u):t===rt?S(n,s):t===ot&&j(n,s))}function C(t,e){try{e(function(e){g(t,e)},function(e){j(t,e)})}catch(n){j(t,n)}}function O(){return ut++}function k(t){t[et]=ut++,t._state=void 0,t._result=void 0,t._subscribers=[]}function Y(t,e){this._instanceConstructor=t,this.promise=new t(p),this.promise[et]||k(this.promise),B(e)?(this._input=e,this.length=e.length,this._remaining=e.length,this._result=new Array(this.length),0===this.length?S(this.promise,this._result):(this.length=this.length||0,this._enumerate(),0===this._remaining&&S(this.promise,this._result))):j(this.promise,q())}function q(){return new Error("Array Methods must be provided an Array")}function F(t){return new Y(this,t).promise}function D(t){var e=this;return new e(B(t)?function(n,r){for(var o=t.length,i=0;i<o;i++){ e.resolve(t[i]).then(n,r) }}:function(t,e){return e(new TypeError("You must pass an array to race."))})}function K(t){var e=this,n=new e(p);return j(n,t),n}function L(){throw new TypeError("You must pass a resolver function as the first argument to the promise constructor")}function N(){throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.")}function U(t){this[et]=O(),this._result=this._state=void 0,this._subscribers=[],p!==t&&("function"!=typeof t&&L(),this instanceof U?C(this,t):N())}function W(){var t=void 0;if("undefined"!=typeof global){ t=global; }else if("undefined"!=typeof self){ t=self; }else { try{t=Function("return this")()}catch(e){throw new Error("polyfill failed because global object is unavailable in this environment")} }var n=t.Promise;if(n){var r=null;try{r=Object.prototype.toString.call(n.resolve())}catch(e){}if("[object Promise]"===r&&!n.cast){ return }}t.Promise=U}var z=void 0;z=Array.isArray?Array.isArray:function(t){return"[object Array]"===Object.prototype.toString.call(t)};var B=z,G=0,H=void 0,I=void 0,J=function(t,e){$[G]=t,$[G+1]=e,G+=2,2===G&&(I?I(a):tt())},Q="undefined"!=typeof window?window:void 0,R=Q||{},V=R.MutationObserver||R.WebKitMutationObserver,X="undefined"==typeof self&&"undefined"!=typeof process&&"[object process]"==={}.toString.call(process),Z="undefined"!=typeof Uint8ClampedArray&&"undefined"!=typeof importScripts&&"undefined"!=typeof MessageChannel,$=new Array(1e3),tt=void 0;tt=X?o():V?s():Z?u():void 0===Q&&"function"==typeof require?f():c();var et=Math.random().toString(36).substring(16),nt=void 0,rt=1,ot=2,it=new M,st=new M,ut=0;return Y.prototype._enumerate=function(){
var this$1 = this;
for(var t=this.length,e=this._input,n=0;this._state===nt&&n<t;n++){ this$1._eachEntry(e[n],n) }},Y.prototype._eachEntry=function(t,e){var n=this._instanceConstructor,r=n.resolve;if(r===h){var o=_(t);if(o===l&&t._state!==nt){ this._settledAt(t._state,e,t._result); }else if("function"!=typeof o){ this._remaining--,this._result[e]=t; }else if(n===U){var i=new n(p);w(i,t,o),this._willSettleAt(i,e)}else { this._willSettleAt(new n(function(e){return e(t)}),e) }}else { this._willSettleAt(r(t),e) }},Y.prototype._settledAt=function(t,e,n){var r=this.promise;r._state===nt&&(this._remaining--,t===ot?j(r,n):this._result[e]=n),0===this._remaining&&S(r,this._result)},Y.prototype._willSettleAt=function(t,e){var n=this;E(t,void 0,function(t){return n._settledAt(rt,e,t)},function(t){return n._settledAt(ot,e,t)})},U.all=F,U.race=D,U.resolve=h,U.reject=K,U._setScheduler=n,U._setAsap=r,U._asap=J,U.prototype={constructor:U,then:l,"catch":function(t){return this.then(null,t)}},W(),U.polyfill=W,U.Promise=U,U});
(function(){if(window.google&&google.gears){return;}
var F=null;if(typeof GearsFactory!='undefined'){F=new GearsFactory();}else{try{F=new ActiveXObject('Gears.Factory');if(F.getBuildInfo().indexOf('ie_mobile')!=-1){F.privateSetGlobalObject(this);}}catch(e){if((typeof navigator.mimeTypes!='undefined')&&navigator.mimeTypes["application/x-googlegears"]){F=document.createElement("object");F.style.display="none";F.width=0;F.height=0;F.type="application/x-googlegears";document.documentElement.appendChild(F);}}}
if(!F){return;}
if(!window.google){google={};}
if(!google.gears){google.gears={factory:F};}})();Persist=(function(){var VERSION='0.3.1',P,B,esc,init,empty,ec;ec=(function(){var EPOCH='Thu, 01-Jan-1970 00:00:01 GMT',RATIO=1000*60*60*24,KEYS=['expires','path','domain'],esc=escape,un=unescape,doc=document,me;var get_now=function(){var r=new Date();r.setTime(r.getTime());return r;};var cookify=function(c_key,c_val){var i,key,val,r=[],opt=(arguments.length>2)?arguments[2]:{};r.push(esc(c_key)+'='+esc(c_val));for(var idx=0;idx<KEYS.length;idx++){key=KEYS[idx];val=opt[key];if(val){r.push(key+'='+val);}}
if(opt.secure){r.push('secure');}
return r.join('; ');};var alive=function(){var k='__EC_TEST__',v=new Date();v=v.toGMTString();this.set(k,v);this.enabled=(this.remove(k)==v);return this.enabled;};me={set:function(key,val){var opt=(arguments.length>2)?arguments[2]:{},now=get_now(),expire_at,cfg={};if(opt.expires){if(opt.expires==-1){cfg.expires=-1}
else{var expires=opt.expires*RATIO;cfg.expires=new Date(now.getTime()+expires);cfg.expires=cfg.expires.toGMTString();}}
var keys=['path','domain','secure'];for(var i=0;i<keys.length;i++){if(opt[keys[i]]){cfg[keys[i]]=opt[keys[i]];}}
var r=cookify(key,val,cfg);doc.cookie=r;return val;},has:function(key){key=esc(key);var c=doc.cookie,ofs=c.indexOf(key+'='),len=ofs+key.length+1,sub=c.substring(0,key.length);return((!ofs&&key!=sub)||ofs<0)?false:true;},get:function(key){key=esc(key);var c=doc.cookie,ofs=c.indexOf(key+'='),len=ofs+key.length+1,sub=c.substring(0,key.length),end;if((!ofs&&key!=sub)||ofs<0){return null;}
end=c.indexOf(';',len);if(end<0){end=c.length;}
return un(c.substring(len,end));},remove:function(k){var r=me.get(k),opt={expires:EPOCH};doc.cookie=cookify(k,'',opt);return r;},keys:function(){var c=doc.cookie,ps=c.split('; '),i,p,r=[];for(var idx=0;idx<ps.length;idx++){p=ps[idx].split('=');r.push(un(p[0]));}
return r;},all:function(){var c=doc.cookie,ps=c.split('; '),i,p,r=[];for(var idx=0;idx<ps.length;idx++){p=ps[idx].split('=');r.push([un(p[0]),un(p[1])]);}
return r;},version:'0.2.1',enabled:false};me.enabled=alive.call(me);return me;}());var index_of=(function(){if(Array.prototype.indexOf){return function(ary,val){return Array.prototype.indexOf.call(ary,val);};}else{return function(ary,val){var i,l;for(var idx=0,len=ary.length;idx<len;idx++){if(ary[idx]==val){return idx;}}
return-1;};}})();empty=function(){};esc=function(str){return'PS'+str.replace(/_/g,'__').replace(/ /g,'_s');};var C={search_order:['localstorage','globalstorage','gears','cookie','ie','flash'],name_re:/^[a-z][a-z0-9_ \-]+$/i,methods:['init','get','set','remove','load','save','iterate'],sql:{version:'1',create:"CREATE TABLE IF NOT EXISTS persist_data (k TEXT UNIQUE NOT NULL PRIMARY KEY, v TEXT NOT NULL)",get:"SELECT v FROM persist_data WHERE k = ?",set:"INSERT INTO persist_data(k, v) VALUES (?, ?)",remove:"DELETE FROM persist_data WHERE k = ?",keys:"SELECT * FROM persist_data"},flash:{div_id:'_persist_flash_wrap',id:'_persist_flash',path:'persist.swf',size:{w:1,h:1},params:{autostart:true}}};B={gears:{size:-1,test:function(){return(window.google&&window.google.gears)?true:false;},methods:{init:function(){var db;db=this.db=google.gears.factory.create('beta.database');db.open(esc(this.name));db.execute(C.sql.create).close();},get:function(key){var r,sql=C.sql.get;var db=this.db;var ret;db.execute('BEGIN').close();r=db.execute(sql,[key]);ret=r.isValidRow()?r.field(0):null;r.close();db.execute('COMMIT').close();return ret;},set:function(key,val){var rm_sql=C.sql.remove,sql=C.sql.set,r;var db=this.db;var ret;db.execute('BEGIN').close();db.execute(rm_sql,[key]).close();db.execute(sql,[key,val]).close();db.execute('COMMIT').close();return val;},remove:function(key){var get_sql=C.sql.get,sql=C.sql.remove,r,val=null,is_valid=false;var db=this.db;db.execute('BEGIN').close();db.execute(sql,[key]).close();db.execute('COMMIT').close();return true;},iterate:function(fn,scope){
var this$1 = this;
var key_sql=C.sql.keys;var r;var db=this.db;r=db.execute(key_sql);while(r.isValidRow()){fn.call(scope||this$1,r.field(0),r.field(1));r.next();}
r.close();}}},globalstorage:{size:5*1024*1024,test:function(){if(window.globalStorage){var domain='127.0.0.1';if(this.o&&this.o.domain){domain=this.o.domain;}
try{var dontcare=globalStorage[domain];return true;}catch(e){if(window.console&&window.console.warn){console.warn("globalStorage exists, but couldn't use it because your browser is running on domain:",domain);}
return false;}}else{return false;}},methods:{key:function(key){return esc(this.name)+esc(key);},init:function(){this.store=globalStorage[this.o.domain];},get:function(key){key=this.key(key);return this.store.getItem(key);},set:function(key,val){key=this.key(key);this.store.setItem(key,val);return val;},remove:function(key){var val;key=this.key(key);val=this.store.getItem[key];this.store.removeItem(key);return val;}}},localstorage:{size:-1,test:function(){try{if(window.localStorage&&window.localStorage.setItem("test",null)==undefined){if(/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)){var ffVersion=RegExp.$1;if(ffVersion>=9){return true;}
if(window.location.protocol=='file:'){return false;}}else{return true;}}else{return false;}
return window.localStorage?true:false;}catch(e){return false;}},methods:{key:function(key){return this.name+'>'+key;},init:function(){this.store=localStorage;},get:function(key){key=this.key(key);return this.store.getItem(key);},set:function(key,val){key=this.key(key);this.store.setItem(key,val);return val;},remove:function(key){var val;key=this.key(key);val=this.store.getItem(key);this.store.removeItem(key);return val;},iterate:function(fn,scope){
var this$1 = this;
var l=this.store,key,keys;for(var i=0;i<l.length;i++){key=l.key(i);keys=key.split('>');if((keys.length==2)&&(keys[0]==this$1.name)){fn.call(scope||this$1,keys[1],l.getItem(key));}}}}},ie:{prefix:'_persist_data-',size:64*1024,test:function(){return window.ActiveXObject?true:false;},make_userdata:function(id){var el=document.createElement('div');el.id=id;el.style.display='none';el.addBehavior('#default#userdata');document.body.appendChild(el);return el;},methods:{init:function(){var id=B.ie.prefix+esc(this.name);this.el=B.ie.make_userdata(id);if(this.o.defer){this.load();}},get:function(key){var val;key=esc(key);if(!this.o.defer){this.load();}
val=this.el.getAttribute(key);return val;},set:function(key,val){key=esc(key);this.el.setAttribute(key,val);if(!this.o.defer){this.save();}
return val;},remove:function(key){var val;key=esc(key);if(!this.o.defer){this.load();}
val=this.el.getAttribute(key);this.el.removeAttribute(key);if(!this.o.defer){this.save();}
return val;},load:function(){this.el.load(esc(this.name));},save:function(){this.el.save(esc(this.name));}}},cookie:{delim:':',size:4000,test:function(){return P.Cookie.enabled?true:false;},methods:{key:function(key){return this.name+B.cookie.delim+key;},get:function(key,fn){var val;key=this.key(key);val=ec.get(key);return val;},set:function(key,val,fn){key=this.key(key);ec.set(key,val,this.o);return val;},remove:function(key,val){var val;key=this.key(key);val=ec.remove(key);return val;}}},flash:{test:function(){try{if(!swfobject){return false;}}catch(e){return false;}
var major=swfobject.getFlashPlayerVersion().major;return(major>=8)?true:false;},methods:{init:function(){if(!B.flash.el){var key,el,fel,cfg=C.flash;el=document.createElement('div');el.id=cfg.div_id;fel=document.createElement('div');fel.id=cfg.id;el.appendChild(fel);document.body.appendChild(el);B.flash.el=swfobject.createSWF({id:cfg.id,data:this.o.swf_path||cfg.path,width:cfg.size.w,height:cfg.size.h},cfg.params,cfg.id);}
this.el=B.flash.el;},get:function(key){var val;key=esc(key);val=this.el.get(this.name,key);return val;},set:function(key,val){var old_val;key=esc(key);old_val=this.el.set(this.name,key,val);return old_val;},remove:function(key){var val;key=esc(key);val=this.el.remove(this.name,key);return val;}}}};init=function(){var i,l,b,key,fns=C.methods,keys=C.search_order;for(var idx=0,len=fns.length;idx<len;idx++){P.Store.prototype[fns[idx]]=empty;}
P.type=null;P.size=-1;for(var idx2=0,len2=keys.length;!P.type&&idx2<len2;idx2++){b=B[keys[idx2]];if(b.test()){P.type=keys[idx2];P.size=b.size;for(key in b.methods){P.Store.prototype[key]=b.methods[key];}}}
P._init=true;};P={VERSION:VERSION,type:null,size:0,add:function(o){B[o.id]=o;C.search_order=[o.id].concat(C.search_order);init();},remove:function(id){var ofs=index_of(C.search_order,id);if(ofs<0){return;}
C.search_order.splice(ofs,1);delete B[id];init();},Cookie:ec,Store:function(name,o){if(!C.name_re.exec(name)){throw new Error("Invalid name");}
if(!P.type){throw new Error("No suitable storage found");}
o=o||{};this.name=name;o.domain=o.domain||location.hostname||'localhost';o.domain=o.domain.replace(/:\d+$/,'');o.domain=(o.domain=='localhost')?'':o.domain;this.o=o;o.expires=o.expires||365*2;o.path=o.path||'/';if(this.o.search_order){C.search_order=this.o.search_order;init();}
this.init();}};init();return P;})();;(function () {
	'use strict';

	/**
	 * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
	 *
	 * @codingstandard ftlabs-jsv2
	 * @copyright The Financial Times Limited [All Rights Reserved]
	 * @license MIT License (see LICENSE.txt)
	 */

	/*jslint browser:true, node:true*/
	/*global define, Event, Node*/


	/**
	 * Instantiate fast-clicking listeners on the specified layer.
	 *
	 * @constructor
	 * @param {Element} layer The layer to listen on
	 * @param {Object} [options={}] The options to override the defaults
	 */
	function FastClick(layer, options) {
		var oldOnClick;

		options = options || {};

		/**
		 * Whether a click is currently being tracked.
		 *
		 * @type boolean
		 */
		this.trackingClick = false;


		/**
		 * Timestamp for when click tracking started.
		 *
		 * @type number
		 */
		this.trackingClickStart = 0;


		/**
		 * The element being tracked for a click.
		 *
		 * @type EventTarget
		 */
		this.targetElement = null;


		/**
		 * X-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartX = 0;


		/**
		 * Y-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartY = 0;


		/**
		 * ID of the last touch, retrieved from Touch.identifier.
		 *
		 * @type number
		 */
		this.lastTouchIdentifier = 0;


		/**
		 * Touchmove boundary, beyond which a click will be cancelled.
		 *
		 * @type number
		 */
		this.touchBoundary = options.touchBoundary || 10;


		/**
		 * The FastClick layer.
		 *
		 * @type Element
		 */
		this.layer = layer;

		/**
		 * The minimum time between tap(touchstart and touchend) events
		 *
		 * @type number
		 */
		this.tapDelay = options.tapDelay || 200;

		/**
		 * The maximum time for a tap
		 *
		 * @type number
		 */
		this.tapTimeout = options.tapTimeout || 700;

		if (FastClick.notNeeded(layer)) {
			return;
		}

		// Some old versions of Android don't have Function.prototype.bind
		function bind(method, context) {
			return function() { return method.apply(context, arguments); };
		}


		var methods = ['onMouse', 'onClick', 'onTouchStart', 'onTouchMove', 'onTouchEnd', 'onTouchCancel'];
		var context = this;
		for (var i = 0, l = methods.length; i < l; i++) {
			context[methods[i]] = bind(context[methods[i]], context);
		}

		// Set up event handlers as required
		if (deviceIsAndroid) {
			layer.addEventListener('mouseover', this.onMouse, true);
			layer.addEventListener('mousedown', this.onMouse, true);
			layer.addEventListener('mouseup', this.onMouse, true);
		}

		layer.addEventListener('click', this.onClick, true);
		layer.addEventListener('touchstart', this.onTouchStart, false);
		layer.addEventListener('touchmove', this.onTouchMove, false);
		layer.addEventListener('touchend', this.onTouchEnd, false);
		layer.addEventListener('touchcancel', this.onTouchCancel, false);

		// Hack is required for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
		// which is how FastClick normally stops click events bubbling to callbacks registered on the FastClick
		// layer when they are cancelled.
		if (!Event.prototype.stopImmediatePropagation) {
			layer.removeEventListener = function(type, callback, capture) {
				var rmv = Node.prototype.removeEventListener;
				if (type === 'click') {
					rmv.call(layer, type, callback.hijacked || callback, capture);
				} else {
					rmv.call(layer, type, callback, capture);
				}
			};

			layer.addEventListener = function(type, callback, capture) {
				var adv = Node.prototype.addEventListener;
				if (type === 'click') {
					adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
						if (!event.propagationStopped) {
							callback(event);
						}
					}), capture);
				} else {
					adv.call(layer, type, callback, capture);
				}
			};
		}

		// If a handler is already declared in the element's onclick attribute, it will be fired before
		// FastClick's onClick handler. Fix this by pulling out the user-defined handler function and
		// adding it as listener.
		if (typeof layer.onclick === 'function') {

			// Android browser on at least 3.2 requires a new reference to the function in layer.onclick
			// - the old one won't work if passed to addEventListener directly.
			oldOnClick = layer.onclick;
			layer.addEventListener('click', function(event) {
				oldOnClick(event);
			}, false);
			layer.onclick = null;
		}
	}

	/**
	* Windows Phone 8.1 fakes user agent string to look like Android and iPhone.
	*
	* @type boolean
	*/
	var deviceIsWindowsPhone = navigator.userAgent.indexOf("Windows Phone") >= 0;

	/**
	 * Android requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0 && !deviceIsWindowsPhone;


	/**
	 * iOS requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent) && !deviceIsWindowsPhone;


	/**
	 * iOS 4 requires an exception for select elements.
	 *
	 * @type boolean
	 */
	var deviceIsIOS4 = deviceIsIOS && (/OS 4_\d(_\d)?/).test(navigator.userAgent);


	/**
	 * iOS 6.0-7.* requires the target element to be manually derived
	 *
	 * @type boolean
	 */
	var deviceIsIOSWithBadTarget = deviceIsIOS && (/OS [6-7]_\d/).test(navigator.userAgent);

	/**
	 * BlackBerry requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsBlackBerry10 = navigator.userAgent.indexOf('BB10') > 0;

	/**
	 * Determine whether a given element requires a native click.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element needs a native click
	 */
	FastClick.prototype.needsClick = function(target) {
		switch (target.nodeName.toLowerCase()) {

		// Don't send a synthetic click to disabled inputs (issue #62)
		case 'button':
		case 'select':
		case 'textarea':
			if (target.disabled) {
				return true;
			}

			break;
		case 'input':

			// File inputs need real clicks on iOS 6 due to a browser bug (issue #68)
			if ((deviceIsIOS && target.type === 'file') || target.disabled) {
				return true;
			}

			break;
		case 'label':
		case 'iframe': // iOS8 homescreen apps can prevent events bubbling into frames
		case 'video':
			return true;
		}

		return (/\bneedsclick\b/).test(target.className);
	};


	/**
	 * Determine whether a given element requires a call to focus to simulate click into element.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element requires a call to focus to simulate native click.
	 */
	FastClick.prototype.needsFocus = function(target) {
		switch (target.nodeName.toLowerCase()) {
		case 'textarea':
			return true;
		case 'select':
			return !deviceIsAndroid;
		case 'input':
			switch (target.type) {
			case 'button':
			case 'checkbox':
			case 'file':
			case 'image':
			case 'radio':
			case 'submit':
				return false;
			}

			// No point in attempting to focus disabled inputs
			return !target.disabled && !target.readOnly;
		default:
			return (/\bneedsfocus\b/).test(target.className);
		}
	};


	/**
	 * Send a click event to the specified element.
	 *
	 * @param {EventTarget|Element} targetElement
	 * @param {Event} event
	 */
	FastClick.prototype.sendClick = function(targetElement, event) {
		var clickEvent, touch;

		// On some Android devices activeElement needs to be blurred otherwise the synthetic click will have no effect (#24)
		if (document.activeElement && document.activeElement !== targetElement) {
			document.activeElement.blur();
		}

		touch = event.changedTouches[0];

		// Synthesise a click event, with an extra attribute so it can be tracked
		clickEvent = document.createEvent('MouseEvents');
		clickEvent.initMouseEvent(this.determineEventType(targetElement), true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
		clickEvent.forwardedTouchEvent = true;
		targetElement.dispatchEvent(clickEvent);
	};

	FastClick.prototype.determineEventType = function(targetElement) {

		//Issue #159: Android Chrome Select Box does not open with a synthetic click event
		if (deviceIsAndroid && targetElement.tagName.toLowerCase() === 'select') {
			return 'mousedown';
		}

		return 'click';
	};


	/**
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.focus = function(targetElement) {
		var length;

		// Issue #160: on iOS 7, some input elements (e.g. date datetime month) throw a vague TypeError on setSelectionRange. These elements don't have an integer value for the selectionStart and selectionEnd properties, but unfortunately that can't be used for detection because accessing the properties also throws a TypeError. Just check the type instead. Filed as Apple bug #15122724.
		if (deviceIsIOS && targetElement.setSelectionRange && targetElement.type.indexOf('date') !== 0 && targetElement.type !== 'time' && targetElement.type !== 'month') {
			length = targetElement.value.length;
			targetElement.setSelectionRange(length, length);
		} else {
			targetElement.focus();
		}
	};


	/**
	 * Check whether the given target element is a child of a scrollable layer and if so, set a flag on it.
	 *
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.updateScrollParent = function(targetElement) {
		var scrollParent, parentElement;

		scrollParent = targetElement.fastClickScrollParent;

		// Attempt to discover whether the target element is contained within a scrollable layer. Re-check if the
		// target element was moved to another parent.
		if (!scrollParent || !scrollParent.contains(targetElement)) {
			parentElement = targetElement;
			do {
				if (parentElement.scrollHeight > parentElement.offsetHeight) {
					scrollParent = parentElement;
					targetElement.fastClickScrollParent = parentElement;
					break;
				}

				parentElement = parentElement.parentElement;
			} while (parentElement);
		}

		// Always update the scroll top tracker if possible.
		if (scrollParent) {
			scrollParent.fastClickLastScrollTop = scrollParent.scrollTop;
		}
	};


	/**
	 * @param {EventTarget} targetElement
	 * @returns {Element|EventTarget}
	 */
	FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {

		// On some older browsers (notably Safari on iOS 4.1 - see issue #56) the event target may be a text node.
		if (eventTarget.nodeType === Node.TEXT_NODE) {
			return eventTarget.parentNode;
		}

		return eventTarget;
	};


	/**
	 * On touch start, record the position and scroll offset.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchStart = function(event) {
		var targetElement, touch, selection;

		// Ignore multiple touches, otherwise pinch-to-zoom is prevented if both fingers are on the FastClick element (issue #111).
		if (event.targetTouches.length > 1) {
			return true;
		}

		targetElement = this.getTargetElementFromEventTarget(event.target);
		touch = event.targetTouches[0];

		if (deviceIsIOS) {

			// Only trusted events will deselect text on iOS (issue #49)
			selection = window.getSelection();
			if (selection.rangeCount && !selection.isCollapsed) {
				return true;
			}

			if (!deviceIsIOS4) {

				// Weird things happen on iOS when an alert or confirm dialog is opened from a click event callback (issue #23):
				// when the user next taps anywhere else on the page, new touchstart and touchend events are dispatched
				// with the same identifier as the touch event that previously triggered the click that triggered the alert.
				// Sadly, there is an issue on iOS 4 that causes some normal touch events to have the same identifier as an
				// immediately preceeding touch event (issue #52), so this fix is unavailable on that platform.
				// Issue 120: touch.identifier is 0 when Chrome dev tools 'Emulate touch events' is set with an iOS device UA string,
				// which causes all touch events to be ignored. As this block only applies to iOS, and iOS identifiers are always long,
				// random integers, it's safe to to continue if the identifier is 0 here.
				if (touch.identifier && touch.identifier === this.lastTouchIdentifier) {
					event.preventDefault();
					return false;
				}

				this.lastTouchIdentifier = touch.identifier;

				// If the target element is a child of a scrollable layer (using -webkit-overflow-scrolling: touch) and:
				// 1) the user does a fling scroll on the scrollable layer
				// 2) the user stops the fling scroll with another tap
				// then the event.target of the last 'touchend' event will be the element that was under the user's finger
				// when the fling scroll was started, causing FastClick to send a click event to that layer - unless a check
				// is made to ensure that a parent layer was not scrolled before sending a synthetic click (issue #42).
				this.updateScrollParent(targetElement);
			}
		}

		this.trackingClick = true;
		this.trackingClickStart = event.timeStamp;
		this.targetElement = targetElement;

		this.touchStartX = touch.pageX;
		this.touchStartY = touch.pageY;

		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			event.preventDefault();
		}

		return true;
	};


	/**
	 * Based on a touchmove event object, check whether the touch has moved past a boundary since it started.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.touchHasMoved = function(event) {
		var touch = event.changedTouches[0], boundary = this.touchBoundary;

		if (Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary) {
			return true;
		}

		return false;
	};


	/**
	 * Update the last position.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchMove = function(event) {
		if (!this.trackingClick) {
			return true;
		}

		// If the touch has moved, cancel the click tracking
		if (this.targetElement !== this.getTargetElementFromEventTarget(event.target) || this.touchHasMoved(event)) {
			this.trackingClick = false;
			this.targetElement = null;
		}

		return true;
	};


	/**
	 * Attempt to find the labelled control for the given label element.
	 *
	 * @param {EventTarget|HTMLLabelElement} labelElement
	 * @returns {Element|null}
	 */
	FastClick.prototype.findControl = function(labelElement) {

		// Fast path for newer browsers supporting the HTML5 control attribute
		if (labelElement.control !== undefined) {
			return labelElement.control;
		}

		// All browsers under test that support touch events also support the HTML5 htmlFor attribute
		if (labelElement.htmlFor) {
			return document.getElementById(labelElement.htmlFor);
		}

		// If no for attribute exists, attempt to retrieve the first labellable descendant element
		// the list of which is defined here: http://www.w3.org/TR/html5/forms.html#category-label
		return labelElement.querySelector('button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea');
	};


	/**
	 * On touch end, determine whether to send a click event at once.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchEnd = function(event) {
		var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;

		if (!this.trackingClick) {
			return true;
		}

		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			this.cancelNextClick = true;
			return true;
		}

		if ((event.timeStamp - this.trackingClickStart) > this.tapTimeout) {
			return true;
		}

		// Reset to prevent wrong click cancel on input (issue #156).
		this.cancelNextClick = false;

		this.lastClickTime = event.timeStamp;

		trackingClickStart = this.trackingClickStart;
		this.trackingClick = false;
		this.trackingClickStart = 0;

		// On some iOS devices, the targetElement supplied with the event is invalid if the layer
		// is performing a transition or scroll, and has to be re-detected manually. Note that
		// for this to function correctly, it must be called *after* the event target is checked!
		// See issue #57; also filed as rdar://13048589 .
		if (deviceIsIOSWithBadTarget) {
			touch = event.changedTouches[0];

			// In certain cases arguments of elementFromPoint can be negative, so prevent setting targetElement to null
			targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement;
			targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent;
		}

		targetTagName = targetElement.tagName.toLowerCase();
		if (targetTagName === 'label') {
			forElement = this.findControl(targetElement);
			if (forElement) {
				this.focus(targetElement);
				if (deviceIsAndroid) {
					return false;
				}

				targetElement = forElement;
			}
		} else if (this.needsFocus(targetElement)) {

			// Case 1: If the touch started a while ago (best guess is 100ms based on tests for issue #36) then focus will be triggered anyway. Return early and unset the target element reference so that the subsequent click will be allowed through.
			// Case 2: Without this exception for input elements tapped when the document is contained in an iframe, then any inputted text won't be visible even though the value attribute is updated as the user types (issue #37).
			if ((event.timeStamp - trackingClickStart) > 100 || (deviceIsIOS && window.top !== window && targetTagName === 'input')) {
				this.targetElement = null;
				return false;
			}

			this.focus(targetElement);
			this.sendClick(targetElement, event);

			// Select elements need the event to go through on iOS 4, otherwise the selector menu won't open.
			// Also this breaks opening selects when VoiceOver is active on iOS6, iOS7 (and possibly others)
			if (!deviceIsIOS || targetTagName !== 'select') {
				this.targetElement = null;
				event.preventDefault();
			}

			return false;
		}

		if (deviceIsIOS && !deviceIsIOS4) {

			// Don't send a synthetic click event if the target element is contained within a parent layer that was scrolled
			// and this tap is being used to stop the scrolling (usually initiated by a fling - issue #42).
			scrollParent = targetElement.fastClickScrollParent;
			if (scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) {
				return true;
			}
		}

		// Prevent the actual click from going though - unless the target node is marked as requiring
		// real clicks or if it is in the whitelist in which case only non-programmatic clicks are permitted.
		if (!this.needsClick(targetElement)) {
			event.preventDefault();
			this.sendClick(targetElement, event);
		}

		return false;
	};


	/**
	 * On touch cancel, stop tracking the click.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.onTouchCancel = function() {
		this.trackingClick = false;
		this.targetElement = null;
	};


	/**
	 * Determine mouse events which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onMouse = function(event) {

		// If a target element was never set (because a touch event was never fired) allow the event
		if (!this.targetElement) {
			return true;
		}

		if (event.forwardedTouchEvent) {
			return true;
		}

		// Programmatically generated events targeting a specific element should be permitted
		if (!event.cancelable) {
			return true;
		}

		// Derive and check the target element to see whether the mouse event needs to be permitted;
		// unless explicitly enabled, prevent non-touch click events from triggering actions,
		// to prevent ghost/doubleclicks.
		if (!this.needsClick(this.targetElement) || this.cancelNextClick) {

			// Prevent any user-added listeners declared on FastClick element from being fired.
			if (event.stopImmediatePropagation) {
				event.stopImmediatePropagation();
			} else {

				// Part of the hack for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
				event.propagationStopped = true;
			}

			// Cancel the event
			event.stopPropagation();
			event.preventDefault();

			return false;
		}

		// If the mouse event is permitted, return true for the action to go through.
		return true;
	};


	/**
	 * On actual clicks, determine whether this is a touch-generated click, a click action occurring
	 * naturally after a delay after a touch (which needs to be cancelled to avoid duplication), or
	 * an actual click which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onClick = function(event) {
		var permitted;

		// It's possible for another FastClick-like library delivered with third-party code to fire a click event before FastClick does (issue #44). In that case, set the click-tracking flag back to false and return early. This will cause onTouchEnd to return early.
		if (this.trackingClick) {
			this.targetElement = null;
			this.trackingClick = false;
			return true;
		}

		// Very odd behaviour on iOS (issue #18): if a submit element is present inside a form and the user hits enter in the iOS simulator or clicks the Go button on the pop-up OS keyboard the a kind of 'fake' click event will be triggered with the submit-type input element as the target.
		if (event.target.type === 'submit' && event.detail === 0) {
			return true;
		}

		permitted = this.onMouse(event);

		// Only unset targetElement if the click is not permitted. This will ensure that the check for !targetElement in onMouse fails and the browser's click doesn't go through.
		if (!permitted) {
			this.targetElement = null;
		}

		// If clicks are permitted, return true for the action to go through.
		return permitted;
	};


	/**
	 * Remove all FastClick's event listeners.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.destroy = function() {
		var layer = this.layer;

		if (deviceIsAndroid) {
			layer.removeEventListener('mouseover', this.onMouse, true);
			layer.removeEventListener('mousedown', this.onMouse, true);
			layer.removeEventListener('mouseup', this.onMouse, true);
		}

		layer.removeEventListener('click', this.onClick, true);
		layer.removeEventListener('touchstart', this.onTouchStart, false);
		layer.removeEventListener('touchmove', this.onTouchMove, false);
		layer.removeEventListener('touchend', this.onTouchEnd, false);
		layer.removeEventListener('touchcancel', this.onTouchCancel, false);
	};


	/**
	 * Check whether FastClick is needed.
	 *
	 * @param {Element} layer The layer to listen on
	 */
	FastClick.notNeeded = function(layer) {
		var metaViewport;
		var chromeVersion;
		var blackberryVersion;
		var firefoxVersion;

		// Devices that don't support touch don't need FastClick
		if (typeof window.ontouchstart === 'undefined') {
			return true;
		}

		// Chrome version - zero for other browsers
		chromeVersion = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];

		if (chromeVersion) {

			if (deviceIsAndroid) {
				metaViewport = document.querySelector('meta[name=viewport]');

				if (metaViewport) {
					// Chrome on Android with user-scalable="no" doesn't need FastClick (issue #89)
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// Chrome 32 and above with width=device-width or less don't need FastClick
					if (chromeVersion > 31 && document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}

			// Chrome desktop doesn't need FastClick (issue #15)
			} else {
				return true;
			}
		}

		if (deviceIsBlackBerry10) {
			blackberryVersion = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/);

			// BlackBerry 10.3+ does not require Fastclick library.
			// https://github.com/ftlabs/fastclick/issues/251
			if (blackberryVersion[1] >= 10 && blackberryVersion[2] >= 3) {
				metaViewport = document.querySelector('meta[name=viewport]');

				if (metaViewport) {
					// user-scalable=no eliminates click delay.
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// width=device-width (or less than device-width) eliminates click delay.
					if (document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}
			}
		}

		// IE10 with -ms-touch-action: none or manipulation, which disables double-tap-to-zoom (issue #97)
		if (layer.style.msTouchAction === 'none' || layer.style.touchAction === 'manipulation') {
			return true;
		}

		// Firefox version - zero for other browsers
		firefoxVersion = +(/Firefox\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];

		if (firefoxVersion >= 27) {
			// Firefox 27+ does not have tap delay if the content is not zoomable - https://bugzilla.mozilla.org/show_bug.cgi?id=922896

			metaViewport = document.querySelector('meta[name=viewport]');
			if (metaViewport && (metaViewport.content.indexOf('user-scalable=no') !== -1 || document.documentElement.scrollWidth <= window.outerWidth)) {
				return true;
			}
		}

		// IE11: prefixed -ms-touch-action is no longer supported and it's recomended to use non-prefixed version
		// http://msdn.microsoft.com/en-us/library/windows/apps/Hh767313.aspx
		if (layer.style.touchAction === 'none' || layer.style.touchAction === 'manipulation') {
			return true;
		}

		return false;
	};


	/**
	 * Factory method for creating a FastClick object
	 *
	 * @param {Element} layer The layer to listen on
	 * @param {Object} [options={}] The options to override the defaults
	 */
	FastClick.attach = function(layer, options) {
		return new FastClick(layer, options);
	};


	if (typeof define === 'function' && typeof define.amd === 'object' && define.amd) {

		// AMD. Register as an anonymous module.
		define(function() {
			return FastClick;
		});
	} else if (typeof module !== 'undefined' && module.exports) {
		module.exports = FastClick.attach;
		module.exports.FastClick = FastClick;
	} else {
		window.FastClick = FastClick;
	}
}());
function getRequest(url, data) {
	return _makeAjaxRequest(url, data, 'GET');
}
function postRequest(url, data) {
	return _makeAjaxRequest(url, data, 'POST');
}
function deleteRequest(url, data) {
	return _makeAjaxRequest(url, data, 'DELETE');
}

function randomString() {
	var LENGTH = 6;
	var CHARS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	var result = '';
	for (var i = 0; i < LENGTH; i++) {
		result += CHARS[Math.random() * CHARS.length | 0];
	}
	return result;
}

function _makeAjaxRequest(url, data, method) {
	if (url[0] === '/') {
		throw new Error('Request URL must not begin with \'/\'.');
	}
	if (!data) { data = {}; }
	if (store.get('userToken')) { data.userToken = store.get('userToken'); }
	if (store.get('providerToken')) { data.providerToken = store.get('providerToken'); }
	if (store.get('adminToken')) { data.adminToken = store.get('adminToken'); }
	if (store.get('enterpriseToken')) { data.enterpriseToken = store.get('enterpriseToken'); }
	data.rnd = randomString(); // to avoid cache

	var params = {
		url: '/' + url,
		type: method,
		data: data,
	};

	if (method == 'POST') {
		params.dataType = 'json';
		params.contentType = 'application/json';
		params.data = JSON.stringify(params.data);
	}

	return $.ajax(params);
}

function downloadAjaxFile(url, data, method) {
	data = data || {};
	method = method || 'post';
	
	if (url) {

		if (store.get('adminToken')) { data.adminToken = store.get('adminToken'); }
		if (store.get('providerToken')) { data.providerToken = store.get('providerToken'); }
		if (store.get('userToken')) { data.userToken = store.get('userToken'); }
		if (store.get('enterpriseToken')) { data.enterpriseToken = store.get('enterpriseToken'); }

		// split params into form inputs
		var inputs = '';

		for (var param in data) {
			inputs += '<input type="hidden" name="' + param + '" value="' + data[param] + '" />';
		}

		//send request
		jQuery('<form action="' + url + '" method="' + method + '">' + inputs + '</form>')
		.appendTo('body').submit().remove();
	}
}
//
// Stream is a queue of values that stores the latest value that has been pushed into it.
//
// To create a stream:
//
// var s = new Stream();
//
// To update ('push') a new value into it:
//
// s.push(123);
//
// To listen to changes of the stream's value:
//
// s.forEach(listener); // e.g.:
// s.forEach(newValue => { console.log('Stream got new value', newValue); });
//
//

function Stream() {
	this.listeners = [];
}

// Does this Stream already have a value?
Stream.prototype.hasValue = function() {
	return this.hasOwnProperty('value');
};

// Add a listener.
//
// If the stream has a value when .forEach() is called, the listener will be called immediately.
// It will be also called for all subsequent values that will be pushed into the stream.
// Listeners must not throw under any circumstances.
Stream.prototype.forEach = function(listener) {
	this.listeners.push(listener);
	this.hasValue() && listener(this.value);
};

// Remove a listener.
// If listener doesn't exist, don't remove.
//
// If you call .forEach() multiple times with the same listener,
// you must call .removeListener() as many times to remove them all.
Stream.prototype.removeListener = function(listener) {
	var index = this.listeners.indexOf(listener);
	index >= 0 && this.listeners.splice(index, 1);
};

// Add a listener that will be called only once.
Stream.prototype.once = function(listener) {
	var self = this;

	function onceListener(value) {
		self.removeListener(onceListener);
		listener(value);
	}

	this.forEach(onceListener);
};

Stream.prototype.push = function(newValue) {
	this.value = newValue;
	// A listener may change this.listeners, so clone it before iterating
	var listeners = [].concat(this.listeners);
	listeners.forEach(function(listener) {
		listener(newValue);
	});
};
var _localStorageNotSupported = false;
try {
	window.localStorage.__test = 'test';
	delete window.localStorage.__test;
} catch (exception) {
	_localStorageNotSupported = true;
}

var originalJSONParse = JSON.parse;
JSON.parse = function(str) {
	if (typeof str == 'string')
		{ return originalJSONParse(str); }
	else
		{ return str; }
};

if (!document.dispatchEvent) // Fastclick breaks on Windows Phone because document.dispatchEvent is missing.
	{ document.dispatchEvent = function() {
}; }

var store = null;

window.onerror = function(errorName, errorFile, line, column, error) {
	errorName = errorName || 'unknown error';
	errorFile = errorFile || 'unknown file';
	line = line || 0;
	column = column || 0;
	function postError(stacktrace) {
		// stacktrace = stacktrace || (errorFile + ':' + line + ':' + column);
		if (typeof postRequest === 'function') {
			postRequest('error', {
				errorName: errorName,
				stacktrace: stacktrace,
				location: location.href,
				platform: 'web',
				file: errorFile,
				line: line,
				column: column,
				userAgent: navigator.userAgent
			});
			log('Error noticed: ' + errorName);
		}
	}

	postError(error && error.stack + (' ' + errorFile + ':' + line + ':' + column));
};

/*

 If you don't know if variable is already fetched:
 wait.env.then(function(){});

 If you know variable has been fetched:
 use.env

 When you have a variable (will resolve the promise):
 waitNoMore.env("value of env");

 Disables variable, can't be waitNoMore'd any more:
 disableVariable.env();

 */
window.wait = {}; // objects whose .then() calls the underlying Stream's .once().
window.use = {}; // values
window.waitNoMore = {}; // functions that push values to the underlying Stream
window.disableVariable = {}; // functions that prevent the Stream from yielding values

function addPromisefulVariable(key) {
	var stream = new Stream();
	var disabled = false;

	wait[key] = {
		then: function(callback) {
			if (disabled) {
				return new Promise(function() {});
			}

			return new Promise(function(resolve) {
				function callbackCaller(value) {
					var result = callback(value);
					resolve(Promise.resolve(result));
				}

				stream.once(callbackCaller);
			});
		}
	};

	waitNoMore[key] = function(value) {
		if (disabled)
			{ return; }

		stream.push(value);
	};

	disableVariable[key] = function() {
		disabled = true;
	};

	Object.defineProperty(use, key, {
		get: function() {
			if (!stream.hasValue()) {
				throw new Error('use.' + key + ' called before waitNoMore.' + key + '!');
			}
			if (disabled) {
				throw new Error('Promiseful variable ' + key + ' is disabled.');
			}
			return stream.value;
		}
	});
}

// You can add more by just calling addPromisefulVariable('myVar');
['env', 'profile', 'googleMaps'].forEach(addPromisefulVariable);

function log() {
	console.log.apply(console, arguments);
}

var userAgent = window.navigator.userAgent;
var isIE = userAgent.indexOf('MSIE ') >= 0 || userAgent.indexOf('Windows Phone') >= 0;
// the easy part from modernizr - good enough
var isTouch = !!(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);

if (window.Persist) {
	store = new Persist.Store('ResQ');
	var originalStoreRemove = store.remove;
	store.remove = function(key) {
		originalStoreRemove.call(store, key);
		if (store.get(key) !== null) {
			// for example safari private mode doesn't support store.remove. set '' instead.
			store.set(key, '');
		}
	};

	if (!_localStorageNotSupported && !store.get('movedToPersist')) {

		// remove extra data
		var localStorageKeysToRemove = ['ResQ>showFirstTimeInfo', 'ResQ>firebase', 'showFirstTimeInfo', 'firebase', 'myNotificationSettings',
			'playNewOrderSound', 'resqZoom', 'resqLatLng', 'resqEnv',
			'adminToken', 'userToken', 'providerToken'];

		Object.keys(localStorage).forEach(function(key) {

			var value = localStorage[key];

			// remove extra data
			var deleted = false;
			localStorageKeysToRemove.forEach(function(str) {
				if (key.indexOf(str) == 0) {
					log('del', key);
					delete localStorage[key];
					deleted = true;
				}
			});

			if (key.indexOf('>') >= 0 || deleted)
				{ return; }
			store.set(key, value);
		});
		store.set('movedToPersist', true);
	}
}

$(function() {
	if (window.Origami)
		{ Origami.fastclick(document.body); }

	window.getRequest && getRequest('env').done(function(response) {
		var envValue = JSON.parse(response);
		waitNoMore.env(envValue);
	});

	if (isTouch) {
		$('html').addClass('touch');
	}

	// Appears to not work
	//if (isIE) {
	//	alert('IE');
	//	$('html').addClass('ie');
	//}
});

function escapedConfirm(message) {
	/**
	 * works just like regular native confirm but returns
	 * true instead of false when the dialog is blocked
	 * from http://stackoverflow.com/a/11571089
	 * @param message
	 * @returns {boolean}
	 */
	var start = new Date().getTime();
	var result = confirm(message);

	var dt = new Date().getTime() - start;
	// dt < 50ms means probable computer
	// the quickest I could get while expecting the popup was 100ms
	// slowest I got from computer suppression was 20ms
	for (var i = 0; i < 10 && !result && dt < 50; i++) {
		start = new Date().getTime();
		result = confirm(message);
		dt = new Date().getTime() - start;
	}
	console.log('escapedDT', dt)
	if (dt < 50)
		{ return true; }
	return result;
}

function redirect(url, replace, calledOnLoad) {
	function doRedirect() {
		if (replace)
			{ location.replace(url); }
		else
			{ location.href = url; }
	}

	if (isIE && calledOnLoad) {
		setTimeout(doRedirect, 2000);
	} else {
		doRedirect();
	}
}

function setLanguageAndRedirect(langCode) {
	// Needed?
	document.cookie = 'resqLang=' + langCode + ';path=/';

	var newPrefix = '/' + langCode + '/';

	var newPathname = document.location.pathname.replace(/^(\/\w\w\/)|\/?/, newPrefix);
	var isLoggedIn = !!use.env.user;
	if (isLoggedIn) {
		postRequest('app/editUser', { language: langCode }).then(function() {
			document.location.pathname = newPathname;
		});
	} else {
		document.location.pathname = newPathname;
	}
}

function getLanguage() {
	var defaultLanguage = window.lang && lang.meta && lang.meta.language;
	var cookieMatch = String(document.cookie).match(/resqLang=([^;]+)/);
	var cookieLanguage = cookieMatch && cookieMatch[1];
	return defaultLanguage || cookieLanguage || 'en';
}

function getLanguageName() {
	return lang.meta.languageNameInLocalLanguage;
}

function formatText(text /*, parameters, ... */) {
	var arguments$1 = arguments;

	if (typeof text === 'object') {
		var num = arguments[1];
		if (num == 0)
			{ text = text.none; }
		else if (num == 1)
			{ text = text.one; }
		else
			{ text = text.many; }
	}

	for (var i = 1; i < arguments.length; i++) {
		text = text.replace(/(%@|%ld|%f|%F)/, arguments$1[i]);
	}

	text = text.replace(/\n/g, '<br/>');

	return text;
}

function offerPriceToFloat(offer) {
	var price = use.env.ENABLE_DYNAMIC_PRICING ? offer.dynamicPrice : offer.price;
	return price / Math.pow(10, offer.provider.country.currency.decimals);
}

function offerToCurrency(offer) {
	return offer.provider.country.currency.id;
}

function offerToCurrencyUpperCase(offer) {
	return offer.provider.country.currency.id.toUpperCase();
}

function formatNumber(number, decimals) {
	decimals = Math.min(10, decimals || 0);
	var decimalSeparator = window.lang ? (lang.format.decimalSeparator || '.') : '.';
	var thousandSeparator = window.lang ? (lang.format.thousandSeparator || '') : '';
	var isNegative = number < 0;

	var numString = (Math.abs(number) + 0.0000000001).toFixed(decimals);

	var split = numString.split('.');
	var mainPart = split[0];
	var decimalPart = split[1];

	for (var i = mainPart.length - 3; i > 0; i -= 3) {
		mainPart = mainPart.substr(0, i) + thousandSeparator + mainPart.substr(i);
	}

	var str = mainPart;
	if (isNegative) { str = '-' + str; }
	if (decimalPart) { str += decimalSeparator + decimalPart; }

	return str;
}

function formatCurrency(cents, currency, plain) {
	if (typeof currency === 'string') {
		var countries = Object.keys(use.env.COUNTRIES).map(function(countryId) {
			return use.env.COUNTRIES[countryId];
		});
		var country = countries.filter(function(country) {
			return country.currency.id === currency;
		})[0];
		if (country)
			{ currency = country.currency; }
	}
	var number;

	// This check is needed on IE 11, find out why!
	if (currency && currency.format) {
		number = formatNumber(cents / currency.unitMultiplier, currency.decimals);
		if (plain) {
			return currency.format.replace('%@', number);
		}
		return currency.format.replace('%@', '<span class="price">' + number + '</span>');
	}
	else {
		number = formatNumber(cents / 100, 2);
		return number;
	}
}

function formatTime(date) {
	return dateFormat(date, window.lang ? lang.format.time : 'HH:mm');
}
function formatDateTime(date) {
	return dateFormat(date, window.lang ? lang.format.dateTime : 'dd.MM.YYYY HH:mm');
}
function formatDate(date) {
	return dateFormat(date, window.lang ? lang.format.date : 'dd.MM.YYYY');
}

function dateFormat(date, formatString) {
	if (!(date instanceof Date))
		{ date = new Date(date); }

	function getMonth(date) {
		return date.getMonth() + 1;
	}

	var formatters = {
		'yyyy': 'getFullYear',
		'M': getMonth,
		'MM': pad2(getMonth),
		'd': 'getDate',
		'dd': pad2('getDate'),
		'h': function(date) {
			return (date.getHours() % 12) || 12;
		},
		'H': 'getHours',
		'HH': pad2('getHours'),
		'm': 'getMinutes',
		'mm': pad2('getMinutes'),
		's': 'getSeconds',
		'ss': pad2('getSeconds'),
		'a': function(date) {
			return date.getHours() < 12 ? 'am' : 'pm';
		},
		'MMM': function(date) {
			if (window.lang) {
				return lang.format.shortMonthNames[date.getMonth()];
			} else {
				return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][date.getMonth()];
			}
		}
	};

	function applyFormatter(date, formatter) {
		return typeof formatter === 'function' ? formatter(date) : date[formatter]();
	}

	function pad2(formatter) {
		return function(date) {
			var result = '' + applyFormatter(date, formatter);
			while (result.length < 2) {
				result = '0' + result;
			}
			return result;
		}
	}

	return formatString.replace(/[a-zA-Z]+/g, function(match) {
		var formatter = formatters[match];
		if (formatter) {
			return applyFormatter(date, formatter);
		}

		console.log('Encountered unknown date formatter', match);
		return match;
	});
}

// TODO should be in providerModalDialog.js?
function dialogFailResponse(response) {
	var text = response.responseText || response;
	showAlert(text).then(function() {
		$('.modalDialog *').attr('disabled', false);
	});
}

$(function() {
	var cm = '';
	var l = false;
	$('body').keypress(function(e) {
		if ($(document.activeElement).prop('tagName') !== 'BODY') {
			cm = '';
			l = false;
		}
		if (e.which == 167) {
			cm = '';
			l = true;
		} else if (l && e.which == 13) {
			var w, z = 'ap3pr8an5at5nw3eeX';
			if (cm.length > 0) {
				for (var i = 0; i < z.length; i += 3) {
					if (cm[0] == z[i] && cm[cm.length - 1] == z[i + 1] && cm.length == z[i + 2].replace('X', '10')) {
						w = z;
						break;
					}
				}
			}
			if (cm.length == 0 || w) {
				var _ = cm;
				var c = [
					'⌐(ಠ۾ಠ)¬',
					' ۜ\\(סּںסּَ` )/ۜ',
					'⨌⨀_⨀⨌',
					'✌(◕‿-)✌',
					'¯\\_(ツ)_/¯ ',
					'•͡˘㇁•͡˘',
					'^⨀ᴥ⨀^',
					'(ಠ_ಠ)',
					'( ͡° ͜ʖ ͡°)﻿',
					'ʕʘ̅͜ʘ̅ʔ',
					'(っ◕‿◕)っ',
					'\\m/_(>_<)_\\m/' ];
				$('body').empty().text(c[~~(Math.random() * c.length)]).append('<br/><br/>' + (_ || 'WOWZA')).css({
					color: 'white',
					background: 'black',
					textAlign: 'center',
					fontSize: '70px',
					paddingTop: '70px',
				});
				setTimeout(function() {
					redirect('/' + _);
				}, 400);
			}
			l = false;
			cm = '';
		} else if (l) {
			cm += String.fromCharCode(e.charCode);
		}

		if (cm.length > 10) {
			cm = '';
			l = false;
		}
	});
});

function clickLinkTo(href) {
	var el = $('<a>').attr({
		'href': href,
		'target': '_blank'
	}).appendTo(document.body);
	el.trigger('click');
	el.remove();
}

function removeHash() {
	if (window.history && history.replaceState) {
		history.replaceState('', document.title,
			window.location.pathname + window.location.search);
	}
}

function trustableHTMLCode(htmlCode) {
	return $('<span>').html(htmlCode);
}

function disableFastClick(elem) {
	$(elem).on('touchstart', function(event) {
		event.target.classList.add('needsclick');
	});
}

function testUserLogin(successCallback, failCallback) {
	if (window.getRequest) {
		getRequest('app/profile', {}).done(function(response) {
			$('.showWhenLoggedOut').hide();
			$('.showWhenLoggedIn').show();

			var myProfile = null;
			try {
				myProfile = JSON.parse(response).profile;
			} catch (e) {
			}

			if (successCallback)
				{ successCallback(myProfile); }
		}).fail(function() {
			if (failCallback)
				{ failCallback(); }
		});
	}
}

//http://stackoverflow.com/questions/18883601/function-to-calculate-distance-between-two-coordinates-shows-wrong
//This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
function calcCrow(lat1, lon1, lat2, lon2) {
	function toRad(Value) {
		return Value * Math.PI / 180;
	}

	var R = 6371; // km
	var dLat = toRad(lat2 - lat1);
	var dLon = toRad(lon2 - lon1);
	var lat1 = toRad(lat1);
	var lat2 = toRad(lat2);

	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var d = R * c;
	return d;
}

function setIntervalUntilElementIsHidden(callback, interval, element) {
	if (!callback || !interval || !element) { return; }

	var first = true;

	function call() {
		if (!first) {
			if (!interval)
				{ return; }

			if ($(element).is(':hidden')) {
				clearInterval(interval);
				interval = null;
				return;
			}
		}

		callback();

		first = false;
	}

	var interval = setInterval(call, interval);
	call();
}

function stringCapitalize(string) {
	if (!string) {
		return '';
	}
	string = string + '';
	return string.charAt(0).toUpperCase() + string.substr(1)
}

function padTo2(input) {
	if (input > 9) {
		return input
	} else {
		return '0' + input;
	}
}
/**
 * * converts a date object to a string without the date
 * @param date
 * @param isHumanReadable return string in format H:M or HH:MM if this is set
 * @returns {string}
 */

function dateToString(date, isHumanReadable) {
	if (!date) { return; }
	date = new Date(date);
	if (!isHumanReadable) {
		return date.getHours() + ':' + date.getMinutes()
	} else {
		return padTo2(date.getHours()) + ':' + padTo2(date.getMinutes())
	}
}

function getLocalDate(offsetMinutes) {
	var localDate = new Date();
	localDate.setMinutes(localDate.getMinutes() + offsetMinutes + localDate.getTimezoneOffset())
	return localDate
}
function getBundleMinutes(minutes) {
	var min = 0;

	if (minutes > 35) {
		min = 65;
	} else if (minutes > 5) {
		min = 35;
	} else {
		min = 5;
	}

	return min;
}
/**
 * Sets up a new date object with the given time and today for a date
 * @param string
 * @returns {*}
 */
function stringToDate(string, options) {
	//console.log("splitting string", string)
	string = string + '';
	string = string.trim();
	var date;

	if (string.length < 4) {
		date = new Date();
		date.setHours(parseInt(string.substr(0, 2)));
		date.setMinutes(0)
		date.setSeconds(0);
		date.setMilliseconds(0);
		return date;
	}

	if (!string || !string.split(':')) {
		console.error('error splitting string', string)
		return false;
	}

	//allow dates in format hh:mm and hh:mm:ss (discard seconds)
	var regex = /([01]\d|2[0-3]):([0-5]\d)/;

	var parts = string.match(regex);
	//"12:01".match(/(\d{1,2}):(\d{1,2})?(?::)?(\d{1,2})/)
	if (parts && parts.length === 3) {
		date = new Date();
		date.setHours(parts[1]);
		date.setMinutes(parts[2])
		date.setSeconds(0);
		date.setMilliseconds(0);
	}

	if (options && options.format === 'utc') {
		date.setMinutes(date.getMinutes() - myInfo.timezoneOffsetInMinutes - date.getTimezoneOffset())
	}

	return date;
}

function sortBy(array, propertyName, reversed) {
	array = array.sort(function(a, b) {
		if (a[propertyName] > b[propertyName]) {
			return 1;
		}
		if (a[propertyName] < b[propertyName]) {
			return -1;
		}
		// a must be equal to b
		return 0;
	});
	if (reversed)
		{ array.reverse(); }
	return array;
}

window.debounce = function(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) { func.apply(context, args); }
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) { func.apply(context, args); }
	};
};

window.extendClassAfterwards = function(mainClass, extendedClass) {
	var mainPrototype = mainClass.prototype;
	var extendedPrototype = new extendedClass;
	Object.assign(extendedPrototype, mainPrototype);
	mainClass.prototype = extendedPrototype;
};

if (!Object.values) {
	Object.values = function(object) {
		var values = [];
		for (var key in object) {
			values.push(object[key]);
		}
		return values;
	}
}

if (typeof Object.assign != 'function') {
	(function() {
		Object.assign = function(target) {
			'use strict';
			var arguments$1 = arguments;

			// We must check against these specific cases.
			if (target === undefined || target === null) {
				throw new TypeError('Cannot convert undefined or null to object');
			}

			var output = Object(target);
			for (var index = 1; index < arguments.length; index++) {
				var source = arguments$1[index];
				if (source !== undefined && source !== null) {
					for (var nextKey in source) {
						if (source.hasOwnProperty(nextKey)) {
							output[nextKey] = source[nextKey];
						}
					}
				}
			}
			return output;
		};
	})();
}

// new Set(array) not supported by IE, gotta use this
window.fromArrayToObjectSet = function(array) {
	var set = {};
	if (array) {
		array.forEach(function(item) {
			set[item] = true;
		});
	}
	return set;
};

/**
 *
 * @param tags
 * @returns {*}
 */
function tagsToString(tags) {
	if (tags && tags.length > 0){
		return tags.map(function(tag){
			return tag.id
		}).join(', ')
	}
	return '';
}

function getAmplitude() {
	// TODO detect iOS and use it
	var eventParameters = {
		platform: 'web'
	};

	var logEvent = function(eventName, params, optCallback) {
		var actualEventParams = $.extend(eventParameters, params);
		// console.log("logged event", eventName, actualEventParams);
		amplitude.logEvent(eventName, actualEventParams, optCallback);
	};

	return {
		then: function(callback) {
			if (!window.amplitude) {
				return;
			}

			wait.env.then(function(env) {
				if (/^\/([a-z][a-z]\/)?app\//.test(window.location.pathname)) {
					// log('Amplitude init with app');
					var apiKey = env.AMPLITUDE_APIKEY;
				} else if (/^\/([a-z][a-z]\/)?provider\//.test(window.location.pathname)) {
					// log('Amplitude init with partner');
					var apiKey = env.AMPLITUDE_PARTNER_APIKEY;
				} else {
					return;
				}

				//amplitude not initialized with an api key
				if (!(amplitude.getInstance().options && amplitude.getInstance().options.apiKey)) {
					amplitude.init(apiKey);
					callback(amplitude, logEvent);
				} else {
					//already initialized
					callback(amplitude, logEvent);
				}

			});

		}
	}
}

function weAreHiring() {
	join(ResQ).then(saveTheWorld);
}

var ResQ = {
	name: 'ResQ Club',
	location: 'Kaisaniemi, Helsinki',
	employees: 10,
	message: 'We need more web and server coders!',
	requirements: [
		'Fluent javascript and jQuery',
		'Startup-minded',
		'Insight to web UX'
	]
};

function join(company) {
	return new Promise(function(resolve, reject) {
		if (company && company.name == 'ResQ Club')
			{ resolve(company); }
		else
			{ reject(new Error('Bad company')) }
	});
}

function saveTheWorld(company) {
	log(company);

	$('body').empty().css({
		background: 'white',
	});

	var layer = $('<div>').css({
		position: 'fixed',
		top: '0px',
		left: '0px',
		width: '100vw',
		height: '100vh',
		textAlign: 'center',
		background: '#222',
		opacity: 0
	}).appendTo('body');

	var welcome = $('<div>').text('Welcome to').css({
		marginTop: '20vh',
		marginBottom: '10vh',
		color: 'white',
		fontSize: '20px'
	}).appendTo(layer);

	var logo = $('<img>').attr('src', '/common/img/resq_logo.png').css({
		opacity: 0,
		width: '100vw',
	}).appendTo(layer);

	var code = $('<pre>').html(weAreHiring.toString().replace('\n', '<br>')).css({
		opacity: 0,
		textAlign: 'left',
		width: '300px',
		margin: '60px auto',
		color: '#BFBFBF',
		background: 'transparent'
	}).appendTo(layer);

	layer.delay(300).animate({
		opacity: 1
	}, 1000, function() {
		logo.delay(500).animate({
			opacity: 1,
			width: '300px'
		}, 150, function() {
			code.delay(1000).animate({
				opacity: 1
			});
		});
	});
}

var SHOW_RECRUITMENT_AD = false;
if (SHOW_RECRUITMENT_AD) {
	var resq =
		'                                                                  ,,,,,,,,,.                \n' +
		'                                                                ,,,,,,,,,,,,,,              \n' +
		' +++++++++++++`                                               ,,,,,,,,,,,,,,,,,             \n' +
		'.+++++++++++++++                                             ,,,,,,,`    .,,,,,,`           \n' +
		'.+++      .\'+++++                                           ,,,,,,          ,,,,,           \n' +
		'.+++         \'++++                                         .,,,,`            ,,,,,          \n' +
		'.+++          ++++                                         ,,,,               ,,,,,         \n' +
		'.+++          :+++          :;++;,            .;++;:      ,,,,.                ,,,,         \n' +
		'.+++          ;+++       .++++++++++        \'+++++++++    ,,,,                  ,,,,        \n' +
		'.+++          ++++      +++++++++++++`     +++++++++++    ,,,`                  ,,,,        \n' +
		'.+++         \'+++`     \'++++      ++++    ,+++.     `    ,,,,                   .,,,        \n' +
		'.+++        ++++:      ++++        ++++   ++++           ,,,,                    ,,,        \n' +
		'.++++++++++++++       \'+++         :+++   ++++           ,,,,           ,        ,,,`       \n' +
		'.+++++++++++++:       ++++          +++   ,++++`         ,,,,         ,. ,       ,,,`       \n' +
		'.+++::::;+++++++      +++;          +++.   +++++++:      ,,,,        , ,, ,      ,,,`       \n' +
		'.+++        ++++      +++++++++++++++++.    ,++++++++    ,,,,         , ,, ,`    ,,,        \n' +
		'.+++         ++++     +++++++++++++++++        .++++++`  `,,,          , ,, ,`  ,,,,        \n' +
		'.+++         ,+++`    +++;                        ,++++   ,,,,          , ,,,,  ,,,,        \n' +
		'.+++          ++++    ++++                         ,+++   ,,,,           , ,,  ,,,,`        \n' +
		'.+++          :+++`   ++++                          +++    ,,,,          `,, , ,,,,         \n' +
		'.+++           ++++    ++++                        `+++    ,,,,,              ,,,,`         \n' +
		'.+++           :+++    +++++        ++`   \'+`      ++++     ,,,,,           `, ,,,          \n' +
		'.+++            ++++    +++++++++++++++   ++++++++++++       ,,,,,,       `,,,``,,.         \n' +
		'.+++            ;+++     .++++++++++++    :++++++++++         ,,,,,,,,,,,,,,,,, ,,,,        \n' +
		' ::.             ,::        .;+++;,          .;++\',            ,,,,,,,,,,,,,,,,  ,,,,`      \n' +
		'                                                                `,,,,,,,,,,,,     ,,,,      \n' +
		'                                                                   .,,,,,,`        ,,,,     \n' +
		'                                                                                    ,,      \n';

	console.log('%c' + resq, 'letter-spacing: 20px; color: #a0c86e; font-weight: bold;');
	console.log('%cWe are hiring!', 'font-size: 30px; color: #a0c86e; font-weight: bold;');
	console.log('%cSend us a POST request', 'font-size: 20px; color: #a0c86e;');
}
//# sourceMappingURL=bundle.js.map