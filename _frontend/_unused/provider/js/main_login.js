var ROOT_PATH = '../';

$(function () {
	var placeholder = '';
	for (var i = 0; i < 6; i++) {
		placeholder += '*';
	}
	
	wait.env.then(function(env) {
		if (env.provider)
			redirect('./', 'replace');
	});

	createTopBar();

	var dlg = new ModalDialog(lang.p.login.title, {
		submitFromEnter: true, horizontalLine: false, popup: false, width: '300px'
		// Not needed as long as we use dlg.addInputOld, which doesn't initialize
		// tabindexes to -1
		// autoTabIndexify: true,

	});

	dlg.getDialogElement().css({
	'border': '1px solid rgba(0,0,0,0.1)',
	'background': 'white'
	});

	dlg.getDialogElement().find('.modal-title h2').css({
		'textAlign': 'center'
	})
	var username = dlg.addInputOld('username', lang.p.login.providerUsername.label, '', 'text', '');
	dlg.addInputOld('password', lang.p.login.password.label, placeholder, 'password', '');

	dlg.addButton(lang.p.login.login, function(data){
		postRequest('provider/login', data).done(function(results){
			results = JSON.parse(results);
			store.set('providerToken', results.providerToken);
			redirect('./', 'replace');
		}).fail(dialogFailResponse);
	});

	dlg.addTextButton(lang.login.forgotPassword, function() {
		var input = prompt(lang.p.forgotPassword.enterUsernameOrEmail, '');
		if (input) {
			var data = {};
			if (input.indexOf('@') >= 0 && input.indexOf('.') >= 0)
				data.email = input;
			else
				data.username = input;
			postRequest('provider/sendPasswordChangeLinkToEmail', data).done(function() {
				showAlert(lang.login.resetRequested.message, ' ');
			}).fail(function(e) {
				showAlert(e.responseText);
			});
		}
	}).addClass('forgotPasswordLink green');

	var dialog = dlg.getDialogElement().appendTo($('body'));

	var input = dialog.find('input').first();

	if (input.val() == '') input.focus();
});
