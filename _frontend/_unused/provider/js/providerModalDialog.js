
// Show alert. Return promise that resolves after alert has been dismissed.
// The returned promise never rejects.
//
// title: "Hups!" by default
// optionalClass: class to be added to alert, e.g. "alert-wide"
// raw: "raw" if alert is message contains HTML (otherwise it will be escaped). Also jquery element is supported
function showAlert(message, title, optionalClass /* e.g. "alert-wide" */, raw, okText) {
	if (title == null) {
		title = window.lang ? lang.oops : 'Oops!';
	}

	if (!okText) {
		okText = 'OK';
	}

	var promise = $.Deferred();

	var alertBackground = $('<div class=alert-background>').click(function(ev) {
		if (ev.target === this) {
			dismiss();
		}
	});

	function dismiss() {
		alertBackground.remove();
		promise.resolve();
	}
	var alertWrapper = $('<div class=alertWrapper>').appendTo(alertBackground);
	var alert = $('<div class=alert>').appendTo(alertWrapper);

	if (optionalClass) {
		alert.addClass(optionalClass);
	}
	alert.append($('<h1>' + title + '</h1>'));
	var messageElement = $('<div class=messageAlert>');
	alert.append(messageElement);
	if (raw) {
		messageElement.append(message);
	} else {
		messageElement.text(message);
	}

	alert.append(createBasicButton(okText, dismiss));

	var oldKeyDown = window.onkeydown;
	window.onkeydown = function(e) {
		var ESC = 27;
		if (e.keyCode == ESC) {
			window.onkeydown = oldKeyDown;
			dismiss();
		}
	};

	alertBackground.appendTo(document.body);

	return promise;
}

/*
 var data = {}
 for (var i = 0; i < dlg.values.length; i++) {
 if (!dlg.values[i].valueName) continue;
 if (dlg.values[i].changed || (dlg.values[i].inputBlock && dlg.values[i].inputBlock.data("changed")))
 data[dlg.values[i].valueName] = dlg.values[i]();
 }

 console.log("data", data)
 * */

function ModalDialog(title, opts)
{
	// console.log("modaldialog", opts)
	var defaultOptions = {
		width: 'auto', // content max width. (for example "600px" or "50vw")
		submitFromEnter: false,
		popup: true,
		saveOnlyEdited: false,
		hideCallback: null,
		preventLeaveOnChanges: false,
		class: '',
		hideTitle: false,
		autoTabIndexify: false,
		autofocusFirstInput: false,
	};

	var currentTabIndex = 100;

	opts = $.extend(defaultOptions, opts);
	var self = this;

	var background = $('<div>').addClass('modal-background').click(function(){
		self.hide();
	});
	var dialogContainer = $('<div>').addClass('modalDialog-container').click(function(){
		self.hide();
	});
	var dialog = $(opts.submitFromEnter ? '<form>' : '<div>').addClass('modalDialog').appendTo(dialogContainer);
	if (opts.class) {
		dialog.addClass(opts.class);
	}
	dialog.click(function(e){
		if (opts.popup)
			e.stopPropagation();
	});
	/*
	 var data = {}
	 for (var i = 0; i < dlg.values.length; i++) {
	 if (!dlg.values[i].valueName) continue;
	 if (dlg.values[i].changed || (dlg.values[i].inputBlock && dlg.values[i].inputBlock.data("changed")))
	 data[dlg.values[i].valueName] = dlg.values[i]();
	 }

	 console.log("data", data)
	 * */

	// if (opts.width) dialog.css("width", opts.width);
/*

	*/

	window._dialog = this;

	$(window).on('hashchange', function(e) {

		//return confirm("You sure?")
	});


	if (!opts.popup)
		dialog.addClass('modalDialog-nonpopup');


	this.addHorizontalLine = function(container){
		return $('<div>').addClass('modal-horizontalLine').appendTo(container || body);
	}

	// title = $('<div>'); //NO-OP

	var	title = $('<div>').addClass('modal-title modalPopupSection').append($('<h2>').append(title).addClass(''));

	if (opts.hideTitle == false){
		title.appendTo(dialog)
	}

	if (opts.popup) {
		var closeButton = $('<div>').addClass('modalCloseButton').append($('<img>').attr('src', '/common/img/icon_close.png')).appendTo(dialog).click(function() {
			if (!closeButton.attr('disabled'))
				self.hide();
		});
	}
	var body = $('<div>').addClass('modalBody modalPopupSection').appendTo(dialog);
	var footer = $('<div>').addClass('modalFooter modalPopupSection').appendTo(dialog);

	if (opts.width) {
		dialog.find('.modalPopupSection').css({
			maxWidth: opts.width,
		});

		//includes 40 + 40 px padding
		dialog.css({maxWidth: 'calc(' + opts.width + ' + 80px)'});

		dialog.append($('<div>').width(opts.width).attr('reason', 'this block defines desired width'));
	}

	var values = [];
	this.values = values;

	var layoutWidth = 12; // align width, not real width
	var i = 0; // align position
	this.confirmLeave = function(){
		if (opts.preventLeaveOnChanges){
			return escapedConfirm(lang.p.dialog.unsavedChanges.confirmText)
		}
		return;

	}
	this.hasChangedValues = function(){
		if (!opts.preventLeaveOnChanges){
			return false;
		}
		var dlg = this;
		$('input:focus').blur();

		var data = {}
		for (var i = 0; i < dlg.values.length; i++) {
			if (!dlg.values[i].valueName) continue;
			if (dlg.values[i].changed || (dlg.values[i].inputBlock && dlg.values[i].inputBlock.data('changed')))
				data[dlg.values[i].valueName] = dlg.values[i]();
		}
		var changedProviderDLTimeInputs = $('.ProviderDLTimeInput input').toArray().map(function(input){ return $(input).val()}).toString();
		//console.log("checking for changes", this.values, data, changedProviderDLTimeInputs);
		return Object.keys(data).length > 0 || (this.savedProviderDLTimeInputs && changedProviderDLTimeInputs && this.savedProviderDLTimeInputs !== changedProviderDLTimeInputs);
	}
	this.addInput = function(options) {
		if (opts.autoTabIndexify && !options.hasOwnProperty('tabIndex'))
			options.tabIndex = currentTabIndex++;

		var appendToFooter = !!options.appendToFooter;
		var block = createInputBlock(options);

		var s = options.size;
		var w = 0; // align width, not real width
		if (s == 'half') {
			w = 6;
			if (i % 6 != 0) this.add($('<br>'));
		}
		else if (s == 'big') w = 11;
		else if (s == 'small') w = 1;
		else if (s == 'auto') w = 1.234567;

		if (!w) i = 0;
		if (i > 0) block.addClass('inputBlock-notFirst');
		i = (i+w) % layoutWidth;

		if (!options.addLater){
			(appendToFooter ? footer : body).append(block);
		}


		if (options.name) {
			var getValueFunc = function() {
				return block.data('get')();
			};
			getValueFunc.valueName = options.name;
			getValueFunc.inputBlock = block;

			values.push(getValueFunc);
		}

		return block;
	};

	this.addInputOld = function(name, title, placeholder, type, defaultValue, horizontalAlign, width) {
		/* FIXME: options does not exist
		
		if (opts.autoTabIndexify)
			options.tabIndex = currentTabIndex++;
			*/

		var div = createInputBlockOld(name, title, placeholder, type, defaultValue, horizontalAlign, width);

		body.append(div);

		var getValueFunc = div.getValueFunc;
		getValueFunc.input = div.input;
		getValueFunc.valueName = name;
		getValueFunc.container = div;
		values.push(getValueFunc);
		return getValueFunc;
	};

	this.addInputTitle = function(title, subTitle, container) {
		container = container || body;
		if (subTitle) subTitle = ' (' + subTitle + ')';
		else subTitle = '';
		var result = $('<div>').addClass('inputBlockTitle').text(title + subTitle);
		result.appendTo(container);
		return result;
	};

	this.add = function(div) {
		if (opts.autoTabIndexify) {
			// Maybe go through div and find its inputs, and give them tabindexes.
			// If necessary.
			// options.tabIndex = currentTabIndex++;
		}

		body.append(div);
		return div;
	};

	this.addToFooter = function(div) {
		footer.append(div);
		return div;
	};

	function createButton(text, callback, optionalExtraClass) {
		return createBasicButton(text, function() {
			var data = {};
			for (var i = 0; i < values.length; i++) {
				if (!values[i].valueName) continue;
				if (!opts.saveOnlyEdited || values[i].changed || (values[i].inputBlock && values[i].inputBlock.data('changed')))
					data[values[i].valueName] = values[i]();
			}

			dialog.find('*').attr('disabled', true);
			callback.apply(null, [data]);
		}, optionalExtraClass).addClass('modal-button');
	}

	this.addBodyButton = function(text, callback, optionalExtraClass) {
		var button = createButton(text, callback, optionalExtraClass);
		body.append(button);
		return button;
	};

	this.addButton = function(text, callback, optionalExtraClass) {
		var button = createButton(text, callback, optionalExtraClass || 'basicConfirmButton');
		footer.append(button);

		if (opts.autoTabIndexify) {
			button.attr('tabindex', currentTabIndex++);
		}

		return button;
	};

	this.addCancelButton = function() {
		console.log('addCancelButton not supported anymore');
		return;
	};

	this.addTextButton = function(text, callback) {
		var button = createTextButton(text, callback);
		
		footer.append($('<div>').css('clear', 'both'), button);
		if (opts.autoTabIndexify) {
			button.attr('tabindex', currentTabIndex++);
		}

		return button;
	};

	this.addClear = function() {
		this.add($('<div>').css('clear', 'both'));
	};
	this.disableAll = function() {
		dialog.find('*').attr('disabled', true);
	};
	this.enableAllAgain = function() {
		dialog.find('*').attr('disabled', false);
	};

	this.show = function() {

		var scrollPosition = document.body.scrollTop;

		var dialog = dialogContainer.children('.modalDialog');

		dialogContainer.css({top: 0})

		dialogContainer.show();

		if ($('html')[0].id !== 'provider'){

			 var topPosition = ~~((window.innerHeight - dialog.height()) / 2);
			 if (topPosition < 70)
			 topPosition = 70;

			 if (topPosition > window.innerHeight*0.25)
			 topPosition = ~~(window.innerHeight*0.25);


			 dialog.css({ top: (scrollPosition + topPosition) + 'px' });

			 dialog.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
				var input = dialog.find('input').first();
				if (input.val() == '') input.focus();
			});

		} else {
			dialog.css({ top: 0 })
		}

		dialogContainer.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
			//fix mobile safaris handling of extra viewport space
			$('html').height(window.innerHeight);
		});



		$('body').append(background, dialogContainer);




		var oldKeyDown = window.onkeydown;
		window.onkeydown = function(e) {
			var ESC = 27;
			if (e.keyCode == ESC) {
				self.hide();
				window.onkeydown = oldKeyDown;
			}
		};
		$('body').addClass('scroll-freeze');
		background.show();

		if (opts.autofocusFirstInput) {
			dialogContainer.find('input, button').first().focus();
		}
	};
	this.hide = function(dismissChanges) {
		if (this.hasChangedValues() && !dismissChanges) {
			if (!this.confirmLeave()) {
				return;
			}
		}

		dialogContainer.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
			dialogContainer.remove();
		});

		$('body').removeClass('scroll-freeze');

		dialogContainer.addClass('modal-hide');

		background.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
			background.remove();
		});
		background.addClass('modal-hide');

		if (opts.hideCallback)
			opts.hideCallback();
	}
	this.getDialogElement = function(){
		return dialog;
	}
	this.isVisible = function() {
		return dialog.is(':visible');
	}

	this.addTimeGraph = function(options){
		var el = this.createTimeGraph(options);
		this.add(el)
	}
	this.createTimeGraph = function(options){
		var timeGraph = createTimeGraph(options);
		//console.log("creating timegraph", timeGraph)
		return timeGraph;
		/*
		var el = $('<div>TimeGraph!</div>');
		el.addClass("providerTimeGraph")
		return el;
		*/
	}

	this.addDLRangeTable = function(options){
		var el = this.createDLRangeTable(options);
		this.add(el);
		return el;
	}
	this.createDLRangeTable = function(options){
		var el = $('<div></div>');
		el.addClass('providerRangeTable');

		var orderRow = createDLRangeRow({type: 'order', callback: options.callback, values: options.values});
		var fetchRow = createDLRangeRow({type: 'fetch', callback: options.callback, values: options.values});
		el.on('change', function(){
			options.callback();
		})
		el.append(orderRow);
		el.append(fetchRow);
		return el;
	}

	this.addDLDaysTable = function(options){
		this.add(this.createDLDaysTable(options));
	}
	this.createDLDaysTable = function(options){
		var el = $('<div/>');
		el.addClass('providerDLRangeTable');

		var icon  = $('<img>').attr('src', '/app/img/Icons_clock.png').addClass('offerDetailsIcon').prop('outerHTML');
		el.append('<div class="providerFetchOrderTimesHeaderRow"><span class="ProviderDLDayRowSpacer"></span><span class="inputBlockTitle">' + icon + ' ' + lang.settings.myInfo.lastOrder + '</span><span class="inputBlockTitle">' + icon + ' ' +  lang.settings.myInfo.lastPickup + '</span></div>')

		for(var i = 0; i < options.days.length ; i++) {
			var row = createDLDayRow({
				day: options['days'][i],
				values: options['values'][i],
				callback: options.callback})
			el.append(row);
		}

		return el;
	}
	this.updateTimeGraph = function(timeGraph, options){
		var values = options.values;
		var firstSlot = options.firstSlot;
		var closingTime = options.closingTime;
		//console.log("updating timeGraph with values", timeGraph, values);
		/*
		values.keys().forEach(function(valueKey){
			var value = values[valueKey];

		})
*/

		var orderMs = 0, fetchMs = 0, orderWidth, orderLeft, fetchWidth, fetchLeft;
		var totalMs = closingTime.getTime() - firstSlot.getTime();
		var currentTimeMs = new Date().getTime() - firstSlot.getTime();

		currentTimeMs = currentTimeMs + (new Date().getTimezoneOffset() + myInfo.timezoneOffsetInMinutes) * 60 * 1000
		//console.info("totalMs", totalMs, closingTime, firstSlot);
		//console.info("currentTimeMs", currentTimeMs / 1000, new Date(), firstSlot);

		if (values.orderStart && values.orderEnd){
			orderMs = values.orderEnd.getTime() - values.orderStart.getTime();
			orderWidth = orderMs / totalMs;
			orderLeft = (values.orderStart.getTime() - firstSlot.getTime()) / totalMs;
		}
		if (values.fetchStart  && values.fetchEnd){
			fetchMs = values.fetchEnd.getTime() - values.fetchStart.getTime();
			fetchWidth = fetchMs / totalMs;
			fetchLeft = (values.fetchStart.getTime() - firstSlot.getTime()) / totalMs;
		} else {
			console.warn('no values found for fetchStart', values)
		}
		var currentTimeLeft = currentTimeMs / totalMs;

		//console.log("updated graph values", orderLeft, orderWidth,fetchLeft, fetchWidth, orderMs, fetchMs, totalMs)

		timeGraph.find('.providerTimeGraphCurrentTime').css({
			left: currentTimeLeft * 100 + '%'
		})

		timeGraph.find('.providerTimeGraphOrder').css({
			width: orderWidth * 100 + '%',
			left: orderLeft * 100 + '%'
		})
		timeGraph.find('.providerTimeGraphFetch').css({
			width: fetchWidth * 100 + '%',
			left: fetchLeft * 100 + '%'
		})

		timeGraph.show();
	}

}
