<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserMailable;
use App\Mail\BusinessMailable;

class EmailController extends Controller
{

  public function send(Request $request)
  {
    $name = $request->input('name');
    $email = $request->input('email');
    $message = $request->input('message');

    Mail::to('noreply@damogo.co.kr')->send(new UserMailable($name, $email, $message));

    return 'ok';
  }

  public function sendBusiness(Request $request)
  {
    $name = $request->input('name');
    $business_name = $request->input('business');
    $email = $request->input('email');
    $business_phone = $request->input('phone');
    $message = $request->input('message');

    Mail::to('noreply@damogo.co.kr')->send(new BusinessMailable($name, $business_name, $email, $business_phone, $message));

    return 'ok';
  }
}
