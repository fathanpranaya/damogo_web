<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Tags\Formatter;

class SubscriberController extends Controller
{
  public function store(Request $request)
  {
    $subscriber = new Subscriber($request->input());
    $subscriber->save();

    return 'ok';

  }

  public function list()
  {
//    return response()->json(Subscriber::all());

    $users = Subscriber::get(); // All users
    $csvExporter = new \Laracsv\Export();
    $csvExporter->build($users, ['email', 'name'])->download();
  }
}
