<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BusinessMailable extends Mailable
{
  use Queueable, SerializesModels;
  public $name, $business_name, $business_phone, $email, $body;

  public function __construct($name, $business_name, $business_phone, $email, $body)
  {
    $this->name = $name;
    $this->business_name = $business_name;
    $this->email = $email;
    $this->business_phone = $business_phone;
    $this->body = $body;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public
  function build()
  {
    return $this->from('business@damogo.co.kr')->view('business_mail');
  }
}
