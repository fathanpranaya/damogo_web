<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserMailable extends Mailable
{
  use Queueable, SerializesModels;
  public $name, $email, $body;


  public function __construct($name, $email, $body)
  {
    $this->name = $name;
    $this->email = $email;
    $this->body = htmlspecialchars($body);
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->from('user@damogo.co.kr')->view('user_mail');
  }
}
