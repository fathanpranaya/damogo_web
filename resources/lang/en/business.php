<?php
/**
 * Created by PhpStorm.
 * User: Msi
 * Date: 10/2/2018
 * Time: 10:23 PM
 */

return [
  'iam' => 'I\'AM',
  'business' => 'BUSINESS',
  'desc' => ' Whether you’re a restaurant, grocery store, convenience store, bakery, coffee shop, cafe, hotel, food truck, or flower store, we know it’s difficult to sell all of your food by the end
                of the day . DamoGO will help you reduce food waste, increase sales, and get new customers. Sounds great right? It IS!',


  'revenue' => 'INCREASE YOUR REVENUE',
  'revenue_desc' => 'Make money on what would otherwise be a 100% loss from throwing away unsold food.',
  'cost' => 'REDUCE COSTS',
  'cost_desc' => 'Less food waste equals less spending on waste management as well as less labor handling the food waste.',
  'customer' => 'REACH NEW CUSTOMERS',
  'customer_desc' => 'Increase your exposure to new customers thru our platform and increase your in-store foot traffic. You’ll also strengthen your brand image by showing your customers that
                            you care about reducing food waste!',
  'impact' => 'CREATE A POSITIVE IMPACT',
  'impact_desc' => 'Reducing food waste reduces greenhouses gases that harm our environment. Show that you care about the planet and benefit from being a socially responsible business. Do good
                            for the planet while doing business.',
  'risk' => 'EASY AND NO RISK',
  'risk_desc' => 'Signing up is free, there are no upfront costs, no monthly payments, no minimum terms, and you can cancel anytime. Payments and transactions go thru our app, and we keep a
                            small percentage of each transaction.',

  'contact' => 'CONTACT',
  'more_info' => 'US FOR MORE INFO',

  'name' => 'Name',
  'business_name' => 'Business Name',
  'business.phone' => 'Business Phone',
  'email' => 'Email',
  'message' => 'Message',
  'submit' => 'Submit',
  'privacy_policy' => 'By clicking you agree to our Terms of Service, Privacy Policy & Refund Policy.'
];
