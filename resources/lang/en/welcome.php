<?php

return [
  'home' => 'Home',
  'how_it_works' => 'How It Works',

  'title_1' => 'Rescue delicious, unsold food from',
  'title_2' => 'being wasted and save up to 80%!',

  'coming_soon' => 'COMING SOON!',

  'submit_email' => 'Get notified on updates by submitting your email below:',

  'subscribe' => 'Join the herd',

  'how' => 'HOW',
  'it_works' => 'IT WORKS',
  'easy_as' => 'It\'s as easy as...',

  'discover' => 'DISCOVER DISCOUNTED FOOD NEARBY',
  'discover_desc' => 'Download and use our app to receive instant push notifications for available
                            discounted food or browse stores nearby (app coming soon).',

  'purchase' => 'PURCHASE',
  'purchase_desc_1' => 'Purchase right on our',
  'purchase_desc_2' => 'app and pick up during',
  'purchase_desc_3' => 'the allotted time!',

  'win' => 'WIN-WIN FOR EVERYONE',
  'win_desc' => 'You save up to 80% on delicious food, businesses earn extra revenue, and we all help to reduce food waste and save the planet!',

  'faq' => 'FAQ',
  'q_1' => 'Is the unsold food safe to eat?',
  'a_1' => 'Yes! This is perfectly good, fresh food you see everyday in restaurants and food stores. The
                        food gets thrown out just because it didn’t get sold by closing time or by the sell
                        by date. It would’ve had a negative impact on the environment had you not rescued it!',

  'q_2' => 'Who are we?',
  'a_2' => 'Restaurants, grocery stores, convenience stores, bakeries, and coffee shops throw away perfectly good food just because it was not sold at the end of the day. Our app is a solution to this ridiculous problem. Fight food waste and help the planet all while saving money on great tasting food from your local stores',

  'about' => 'ABOUT',
  'us' => 'US',
  'our' => 'OUR',
  'mission' => 'MISSION',

  'mission_1' => '1/3 of all food produced in the world is wasted. It’s an alarming statistic that should not be
                        happening when so many people around the world is starving. Almost 14,000 tons of
                        food wasted everyday in Seoul. Yearly food waste in South Korea is counted at a huge 6.3 million
                        of tons. That is not a small number indeed.',
  'mission_2' => 'DamoGO is an app that helps put a dent in the problem. We are going to reduce food waste by
                        helping retailers sell their unsold food at a discount before it gets thrown away.
                        This is perfectly good, delicious food that goes in the trash simply because it was not sold by
                        closing time. Customers can rescue delicious food and help the environment by
                        reducing food waste.',
  'mission_3' => 'This is a true case of a win-win situation. Customers save heaps on delicious food. Stores make
                        extra money on food that would have actually COST them money to throw away. Most
                        importantly, our planet and environment win by reduced greenhouse gases from rotting food waste.
                        Let’s all be part of this movement together!',

  'contact' => 'CONTACT',
  'more_info' => 'US FOR MORE INFO',
  'name' => 'Name',
  'email' => 'Email',
  'message' => 'Message',
  'submit' => 'Submit',
  'privacy_policy' => 'By clicking you agree to our Terms of Service, Privacy Policy & Refund Policy.'

];
