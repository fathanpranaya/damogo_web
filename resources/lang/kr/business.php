<?php
/**
 * Created by PhpStorm.
 * User: Msi
 * Date: 10/2/2018
 * Time: 10:23 PM
 */

return [
  'iam' => 'I\'AM',
  'business' => 'BUSINESS',
  'desc' => ' 여러분께서 운영하는 레스토랑, 식료품가게, 편의점, 베이커리, 커피숍, 카페, 호텔, 푸드 트럭 그리고 꽃집에서 하루의 영업이 끝나는 시점까지 모든 상품을 판매하기란 참 어려운 일이죠. 또, 남은 음식을 온전한 채로 버리 는것은 참으로 가슴 아픈일이 아닐수 없습니다. DamoGO는 미판매분의 음식을 판매하여 전반적인 판매를 증
                가시키며, 음식물 낭비를 줄이는 동시에 음식 폐기물 처리에 대한 비용을 줄이고, 새로운 고객을 창출 하는데 도움을 드립니다.',


  'revenue' => '매출증대',
  'revenue_desc' => '버려야만 하는 미판매 상품을 판매하여 매출을 증가시킬 수 있습니다',
  'cost' => '처리 및 인건 비용절감',
  'cost_desc' => '음식물 폐기물을 줄여, 그에 따른 처리비용과 인건 비용을 줄일 수 있습니다. ',
  'customer' => '새로운 고객 창출',
  'customer_desc' => '다양한 고객들에게 여러분의 매장을 노출시켜 매장 트래픽을 증가시키고, 환경을 생각하는 긍적적인 메장 이미지를 강화시킬 수 있습니다.',
  'impact' => '긍정적 영향 창출',
  'impact_desc' => '음식물 폐기물을 줄이면 우리 환경에 악영향을 끼치는 온실가스를 줄일 수 있습니다. 환경을 생각하는 매장으로 환경 보존에 이바지하며 그에 따른 혜택을 누리세요.',
  'risk' => '쉽고 위험요소 제로',
  'risk_desc' => '업체 누구나 무료로 가입 할 수 있고, 초기비용이나 월별 지불 비용도 없으며 언제든지 등록(가입)을 취소 할 수 있어요. 거래가 이루어지면 어플리케이션을 통해 소비자가 비용을 DamoGO에 지불하게 되며, 그 중 아주 적은 비율의 비용만 DamoGO가 가지게 되요',

  'contact' => 'CONTACT',
  'more_info' => 'US FOR MORE INFO',

  'name' => '이름',
  'business_name' => '상호명',
  'email' => '연락처 또는 이메일',
  'business_phone' => '직장 전화 번호',
  'message' => '메세지',
  'submit' => '등록/가입하기',
  'privacy_policy' => 'By clicking you agree to our Terms of Service, Privacy Policy & Refund Policy.'
];
