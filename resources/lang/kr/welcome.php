<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Pagination Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used by the paginator library to build
  | the simple pagination links. You are free to change them to anything
  | you want to customize your views to better match your application.
  |
  */

  'home' => 'Home',
  'how_it_works' => 'How It Works',

  'title_1' => '맛있고 신선한 음식을 80% 할인된 가격으로 구매하면서',
  'title_2' => '음식물 폐기물로 부터 지구를 보호하세요',

  'q_2' => '우리 회사는?',
  'a_2' => '마트, 편의점, 베이커리 그리고 커피숍에서는 당일 영업이 끝나면 그날 판매되지 않았다는 이유만으로 온전한 음식을 폐기하고 있어요.
                DamaGO는 신선한 채로 버려지는 음식에 대한 해답을 찾았습니다! DamaGO를 통해 주변 매장의 신선하고 맛있는 음식을 할인된 가격으로 저렴하게
                구매하면서 음식물 낭비도 줄여 우리 지 구를 안전하게 지키는일에 동참해주세요.',

  'coming_soon' => '곧 찾아옵니다!',

  'submit_email' => '이메일 주소를 등록하고 다양한 할인 정보를 받아보세요. 이메일 주소 등록하기:',

  'subscribe' => '등록/가입하기',

  'how' => '이용',
  'it_works' => '방법:',
  'easy_as' => '',

  'discover' => 'DISCOVER DISCOUNTED FOOD NEARBY',
  'discover_desc' => '가까운 매장의 할인된 음식을 찾아드려요. 어플리케이션을 다운로드 하고 할인된 상품에 대한 푸쉬알 람을 수신하거나 인근 매장의 할인 상품을
                            검색할수 있습니다.',

  'purchase' => 'PURCHASE',
  'purchase_desc_1' => '마음에 드는 상품을 어플리케이션을',
  'purchase_desc_2' => '통해 바로 구입하고',
  'purchase_desc_3' => '할당된 시간동안 픽업하면 끝!',

  'win' => 'WIN-WIN FOR EVERYONE',
  'win_desc' => '소비자는 신선하고 맛있는 음식을 80% 할인된 가격으로 저렴하게 구입할 수 있고, 매장은 매출 증가 에 대한 이윤을 얻을수 있으며 이것은 우리
                            모두가 음식물 쓰레기를 줄여 지구를 보호할 수 있는 ‘윈- 윈 시츄에이션’입니다.',

  'faq' => 'FAQ',
  'q_1' => '미판매 않은 음식을 먹어도 안전한가요?',
  'a_1' => '네, 그렇습니다. DamaGO에 판매되는 음식들은 레스토랑이나 마트에서 일반적으로 판매되는 음식이에요. 이 런 음식들은 당일에 판매되지 않았다는 이유만으로 하루의 영업이
                        끝나는 시점에 또는 판매기간이 끝나는 시점 에 신선하고 온전한채로 버려지고 있습니다. 이렇게 버려지는 엄청난 양의 음식물 쓰레기로 인해 우리
                        지구는 오늘도 심각하게 오염되고 있어요.',

  'about' => 'Damogo는',
  'us' => '무엇입니까?',
  'our' => '저희의',
  'mission' => '임무',

  'mission_1' => '통계에 따르면 전세계 음식물의 1/3은 버려진다고 해요 세계 곳곳, 먹을것이 없어 굶어 죽어가는 많은 사람들 이 있는 가운데 이런 낭비는 절대 있어서는 안되죠.',
  'mission_2' => 'DamoGO는 이러한 문제에 조금이나마 도움이 될 수 있는 어플리케이션입니다. DamoGO는 음식물 낭비를 줄일 수 있게 하죠. 매장의 미판매분의 음식은 당일에 판매되지
                        않았다는 이유만으로 영업이 끝나는 시점에 신 선하고 온전한채로 버려지게 됩니다. 소비자들은 어플리케이션을 통해 이런 음식들을 저렴하게 구입할 수
                        있 고, 이는 음식 폐기물을줄여 지구를 보호할 수 있습니다.',
  'mission_3' => '이것은 윈-윈 시츄에이션입니다. 소비자는 맛있는 음식을 저렴하게 구입할수 있어서 좋고, 매장은 그만큼의 추 가 매출 올리며, 음식을 버리는데 드는 비용마저 절약할 수 있어서
                        좋죠. 그렇지만 더 중요하게는, 음식물을 썩 힐때 나오는 온실가스를 줄여 우리 모두가 지구 환경을 보존하는일에 동참할 수 있게 되요.',

  'contact' => 'CONTACT',
  'more_info' => 'US FOR MORE INFO',
  'name' => '이름',
  'email' => '이메일',
  'message' => '메세지',
  'submit' => '등록/가입하기',
  'privacy_policy' => 'By clicking you agree to our Terms of Service, Privacy Policy & Refund Policy . '

];
