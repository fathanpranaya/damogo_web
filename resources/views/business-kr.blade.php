<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <title>Home</title>
    <meta name="description" content="Startups template">
    <meta name="keywords" content="Startups template">
    <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
    <link rel="shortcut icon" href="{{asset('startuply/img/favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{asset('startuply/img/apple-touch-icon.jpg')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('startuply/img/apple-touch-icon-72x72.jpg')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('startuply/img/apple-touch-icon-114x114.jpg')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('startuply/css/custom-animations.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('startuply/css/lib/font-awesome.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('startuply/css/style.css')}}"/>

    <!--[if lt IE 9]>
    <script src="{{asset('startuply/js/html5shiv.js')}}"></script>
    <script src="{{asset('startuply/js/respond.min.js')}}"></script>
    <![endif]-->
</head>

<body id="landing-page" class="landing-page">
<!-- Preloader -->
<div class="preloader-mask">
    <div class="preloader">
        <div class="spin base_clr_brd">
            <div class="clip left">
                <div class="circle"></div>
            </div>
            <div class="gap">
                <div class="circle"></div>
            </div>
            <div class="clip right">
                <div class="circle"></div>
            </div>
        </div>
    </div>
</div>

{{--HEADER MENU SECTION--}}
<header class="fixed-menu">
    <nav class="navigation navigation-header white-dropdown">
        <div class="container">
            <div class="navigation-brand">
                <div class="brand-logo">
                    <a href="index.html" class="logo"></a><a href="index.html" class="logo logo-alt"></a>
                </div>
            </div>
            <button class="navigation-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navigation-navbar collapsed">
                <ul class="navigation-bar navigation-bar-left">
                    <li><a href="{{url('/')}}">I'm User</a></li>
                    <li><a href="#business">I'm Business</a></li>
                    <li><a href="#contactus">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<section id="business" class="section features-list-section align-center dark-text">
    <div class="container">
        <div class="section-header">
            <h2 style="margin-top:80px;"><span class="highlight">I'AM</span> BUSINESS</h2>
            <p class="sub-title">
                여러분께서 운영하는 레스토랑, 식료품가게, 편의점, 베이커리, 커피숍, 카페, 호텔, 푸드 트럭 그리고 꽃집에서 하루의 영업이 끝나는 시점까지 모든 상품을 판매하기란 참 어려운 일이죠. 또, 남은 음식을 온전한 채로 버리 는것은 참으로 가슴 아픈일이 아닐수 없습니다. DamoGO는 미판매분의 음식을 판매하여 전반적인 판매를 증
                가시키며, 음식물 낭비를 줄이는 동시에 음식 폐기물 처리에 대한 비용을 줄이고, 새로운 고객을 창출 하는데 도움을 드립니다.
            </p>
        </div>

        <div class="section-content">
            <div class="clearfix animated" data-duration="500" data-animation="fadeInRight">

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-arrows-37 highlight"></i>
                        <span class="heading">매출증대</span>
                        <p class="">버려야만 하는 미판매 상품을 판매하여 매출을 증가시킬 수 있습니다. </p>
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-arrows-38 highlight"></i>
                        <span class="heading">처리 및 인건 비용절감</span>
                        <p class="">음식물 폐기물을 줄여, 그에 따른 처리비용과 인건 비용을 줄일 수 있습니다. </p>
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-seo-icons-24 highlight"></i>
                        <span class="heading">새로운 고객 창출</span>
                        <p class="">
                            다양한 고객들에게 여러분의 매장을 노출시켜 매장 트래픽을 증가시키고, 환경을 생각하는 긍적적인 메장 이미지를 강화시킬 수 있습니다.
                        </p>
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-ecology-01 highlight"></i>
                        <span class="heading">긍정적 영향 창출</span>
                        <p class="">
                            음식물 폐기물을 줄이면 우리 환경에 악영향을 끼치는 온실가스를 줄일 수 있습니다. 환경을 생각하는 매장으로 환경 보존에 이바지하며 그에 따른 혜택을 누리세요.
                        </p>
                    </article>
                </div>
            </div>

            <div class="clearfix animated" data-duration="500" data-delay="500" data-animation="fadeInLeft">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="align-center">
                        <i class="icon icon-badges-votes-01 highlight"></i>
                        <span class="heading">쉽고 위험요소 제로</span>
                        <p class="">
                            업체 누구나 무료로 가입 할 수 있고, 초기비용이나 월별 지불 비용도 없으며 언제든지 등록(가입)을 취소 할 수 있어요. 거래가 이루어지면 어플리케이션을 통해 소비자가 비용을 DamoGO에 지불하게 되며, 그 중 아주 적은 비율의 비용만 DamoGO가 가지게 되요
                        </p>
                    </article>
                </div>
            </div>
        </div>
</section>

<!-- <section id="team" class="section team-section align-center dark-text"> -->
<section id="contactus" class="section features-section align-center inverted">
    <div class="container">
        <h2><span class="highlight">CONTACT</span> US FOR MORE INFO</h2>
        <div class="row">
            <div class="col-lg-7 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
                <form class="form form-register" id="registration" method="post" action="signup.php">
                    <div class="form-group">
                        <label for="fullname" class="col-sm-4 col-xs-12 control-label">이름</label>
                        <div class="col-sm-8 col-xs-12">
                            <input type="text" class="form-control required" name="fullname" id="fullname" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="business" class="col-sm-4 col-xs-12 control-label">상호명</label>
                        <div class="col-sm-8 col-xs-12">
                            <input type="text" class="form-control" name="business" id="business" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-4 col-xs-12 control-label">연락처 또는 이메일</label>
                        <div class="col-sm-8 col-xs-12">
                            <input type="email" class="form-control required email" name="email" id="email" placeholder="user@damogo.com">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-4 col-xs-12 control-label">Phone</label>
                        <div class="col-sm-8 col-xs-12">
                            <input type="text" class="form-control required email" name="phone" id="phone" placeholder="+8210-xxxx-xxxx">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-4 col-xs-12 control-label">메세지</label>
                        <div class="col-sm-8 col-xs-12">
                            <textarea id="message" name="message" class="form-control message" placeholder="Enter your message here"></textarea>
                        </div>
                    </div>
                    <input type="submit" class="btn-solid btn-lg btn-block" value="등록/가입하기"/>
                </form>

                <p class="agree-text align-center">
                    By clicking you agree to our Terms of Service, Privacy Policy & Refund Policy.
                </p>
            </div>
        </div>
    </div>
</section>

<footer id="footer" class="footer light-text">
    <div class="container">
        <div class="footer-content row">
            <div class="col-sm-4 col-xs-12">
                <div class="logo-wrapper">
                    <img width="130" height="31" src="{{asset('startuply/img/logo-white.png')}}" alt="logo"/>
                </div>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco. Qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco. Nisi ut
                    aliquid ex ea commodi consequatur?</p>
                <p><strong>John Doeson, Founder</strong>.</p>
            </div>
            <div class="col-sm-5 social-wrap col-xs-12">
                <strong class="heading">Social Networks</strong>
                <ul class="list-inline socials">
                    <li><a href="#"><span class="icon icon-socialmedia-08"></span></a></li>
                    <li><a href="#"><span class="icon icon-socialmedia-09"></span></a></li>
                    <li><a href="#"><span class="icon icon-socialmedia-16"></span></a></li>
                    <li><a href="#"><span class="icon icon-socialmedia-04"></span></a></li>
                </ul>
                <ul class="list-inline socials">
                    <li><a href="#"><span class="icon icon-socialmedia-07"></span></a></li>
                    <li><a href="#"><span class="icon icon-socialmedia-16"></span></a></li>
                    <li><a href="#"><span class="icon icon-socialmedia-09"></span></a></li>
                    <li><a href="#"><span class="icon icon-socialmedia-08"></span></a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-12">
                <strong class="heading">Our Contacts</strong>
                <ul class="list-unstyled">
                    <li><span class="icon icon-chat-messages-14"></span><a href="mailto:info@startup.ly">info@startup.ly</a></li>
                    <li><span class="icon icon-seo-icons-34"></span>2901 Marmora road, Glassgow, Seattle, WA 98122-1090</li>
                    <li><span class="icon icon-seo-icons-17"></span>1 - 234-456-7980</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="copyright">startup.ly 2014. All rights reserved.</div>
</footer>

<div class="back-to-top"><i class="fa fa-angle-up fa-3x"></i></div>

<!--[if lt IE 9]>
<script type="text/javascript" src="{{asset('startuply/js/jquery-1.11.3.min.js?ver=1')}}"></script>
<![endif]-->
<!--[if (gte IE 9) | (!IE)]><!-->
<script type="text/javascript" src="{{asset('startuply/js/jquery-2.1.4.min.js?ver=1')}}"></script>
<!--<![endif]-->

<script type="text/javascript" src="{{asset('startuply/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.flexslider-min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.appear.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.plugin.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.countdown.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/startuply.js')}}"></script>
</body>
</html>
