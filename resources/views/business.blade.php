@extends('layout')

@section('content')
    <!-- Preloader -->
    <div class="preloader-mask">
        <div class="preloader">
            <div class="spin base_clr_brd">
                <div class="clip left">
                    <div class="circle"></div>
                </div>
                <div class="gap">
                    <div class="circle"></div>
                </div>
                <div class="clip right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>

    {{--HEADER MENU SECTION--}}
    <header class="fixed-menu">
        <nav class="navigation navigation-header white-dropdown">
            <div class="container">
                <div class="navigation-brand">
                    <div class="brand-logo">
                        <a href="{{url('/')}}" class="logo"></a><a href="{{url('/')}}" class="logo logo-alt"></a>
                    </div>
                </div>
                <button class="navigation-toggle">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navigation-navbar collapsed">
                    <ul class="navigation-bar navigation-bar-left">
                        <li><a href="{{url('/')}}">I'm a User</a></li>
                        <li><a href="#business">I'm Business</a></li>
                        <li><a href="#contactus">Contact Us</a></li>
                    </ul>
                    <ul class="navigation-bar navigation-bar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                {{ Config::get('languages')[App::getLocale()] }}
                            </a>
                            <ul class="dropdown-menu">
                                @foreach (Config::get('languages') as $lang => $language)
                                    @if ($lang != App::getLocale())
                                        <li>
                                            <a href="{{ route('lang.switch', $lang) }}">{{$language}}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <section id="business" class="section features-list-section align-center dark-text">
        <div class="container">
            <div class="section-header">
                <h2 style="margin-top:80px;"><span class="highlight">{{trans('business.iam')}}</span> {{trans('business.business')}}</h2>
                <p class="sub-title">
                    {{trans('business.desc')}}
                </p>
            </div>

            <div class="section-content">
                <div class="clearfix animated" data-duration="500" data-animation="fadeInRight">

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <article class="align-center">
                            <i class="icon icon-arrows-37 highlight"></i>
                            <span class="heading">{{trans('business.revenue')}}</span>
                            <p class="">{{trans('business.revenue_desc')}} </p>
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <article class="align-center">
                            <i class="icon icon-arrows-38 highlight"></i>
                            <span class="heading">{{trans('business.cost')}}S</span>
                            <p class="">{{trans('business.cost_desc')}}</p>
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <article class="align-center">
                            <i class="icon icon-seo-icons-24 highlight"></i>
                            <span class="heading">{{trans('business.customer')}}</span>
                            <p class="">
                                {{trans('business.customer_desc')}}
                            </p>
                        </article>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <article class="align-center">
                            <i class="icon icon-ecology-01 highlight"></i>
                            <span class="heading">{{trans('business.impact')}}</span>
                            <p class="">
                                {{trans('business.impact_desc')}}
                            </p>
                        </article>
                    </div>
                </div>

                <div class="clearfix animated" data-duration="500" data-delay="500" data-animation="fadeInLeft">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <article class="align-center">
                            <i class="icon icon-badges-votes-01 highlight"></i>
                            <span class="heading">{{trans('business.risk')}}</span>
                            <p class="">
                                {{trans('business.risk_desc')}}
                            </p>
                        </article>
                    </div>
                </div>
            </div>
    </section>

    <!-- <section id="team" class="section team-section align-center dark-text"> -->
    <section id="contactus" class="section features-section align-center inverted">
        <div class="container">
            <h2><span class="highlight">{{trans('business.contact')}}</span> {{trans('business.more_info')}}</h2>
            <div class="row">
                <div class="col-lg-7 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
                    <form class="form form-register" id="registration" method="post" action="{{url('/sendbusiness')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="fullname" class="col-sm-4 col-xs-12 control-label">{{trans('business.name')}}</label>
                            <div class="col-sm-8 col-xs-12">
                                <input type="text" class="form-control required" name="name" id="name" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="business" class="col-sm-4 col-xs-12 control-label">{{trans('business.business_name')}}</label>
                            <div class="col-sm-8 col-xs-12">
                                <input type="text" class="form-control" name="business" id="business" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-4 col-xs-12 control-label">{{trans('business.email')}}</label>
                            <div class="col-sm-8 col-xs-12">
                                <input type="email" class="form-control required email" name="email" id="email" placeholder="user@damogo.com">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-4 col-xs-12 control-label">{{trans('business.business_phone')}}</label>
                            <div class="col-sm-8 col-xs-12">
                                <input type="text" class="form-control required" name="phone" id="phone" placeholder="+8210-xxxx-xxxx">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-4 col-xs-12 control-label">{{trans('business.message')}}</label>
                            <div class="col-sm-8 col-xs-12">
                                <textarea id="message" name="message" class="form-control message" placeholder="Enter your message here"></textarea>
                            </div>
                        </div>
                        <input type="submit" class="btn-solid btn-lg btn-block" value="{{trans('business.submit')}}"/>
                    </form>

                    <p class="agree-text align-center">
                        {{trans('business.privacy_policy')}}
                    </p>
                </div>
            </div>
        </div>
    </section>

    {{--<footer id="footer" class="footer light-text">--}}
    {{--<div class="container">--}}
    {{--<div class="footer-content row">--}}
    {{--<div class="col-sm-4 col-xs-12">--}}
    {{--<div class="logo-wrapper">--}}
    {{--<img width="130" height="31" src="{{asset('startuply/img/logo-white.png')}}" alt="logo"/>--}}
    {{--</div>--}}
    {{--<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco. Qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco. Nisi ut--}}
    {{--aliquid ex ea commodi consequatur?</p>--}}
    {{--<p><strong>John Doeson, Founder</strong>.</p>--}}
    {{--</div>--}}
    {{--<div class="col-sm-5 social-wrap col-xs-12">--}}
    {{--<strong class="heading">Social Networks</strong>--}}
    {{--<ul class="list-inline socials">--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-08"></span></a></li>--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-09"></span></a></li>--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-16"></span></a></li>--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-04"></span></a></li>--}}
    {{--</ul>--}}
    {{--<ul class="list-inline socials">--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-07"></span></a></li>--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-16"></span></a></li>--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-09"></span></a></li>--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-08"></span></a></li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--<div class="col-sm-3 col-xs-12">--}}
    {{--<strong class="heading">Our Contacts</strong>--}}
    {{--<ul class="list-unstyled">--}}
    {{--<li><span class="icon icon-chat-messages-14"></span><a href="mailto:info@startup.ly">info@startup.ly</a></li>--}}
    {{--<li><span class="icon icon-seo-icons-34"></span>2901 Marmora road, Glassgow, Seattle, WA 98122-1090</li>--}}
    {{--<li><span class="icon icon-seo-icons-17"></span>1 - 234-456-7980</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="copyright">startup.ly 2014. All rights reserved.</div>--}}
    {{--</footer>--}}

    <div class="back-to-top"><i class="fa fa-angle-up fa-3x"></i></div>
@endsection
