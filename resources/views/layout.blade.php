<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta name="google-site-verification" content="YyGEbWLqcOp8b1PSCa8GpszMBncbkO7ZCDyvm2GtJZ0"/>
    <title>Damogo</title>
    <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
    <link rel="shortcut icon" href="{{asset('startuply/img/favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{asset('startuply/img/apple-touch-icon.jpg')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('startuply/img/apple-touch-icon-72x72.jpg')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('startuply/img/apple-touch-icon-114x114.jpg')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('startuply/css/custom-animations.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('startuply/css/lib/font-awesome.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('startuply/css/style.css')}}"/>

    <!--[if lt IE 9]>
    <script src="{{asset('startuply/js/html5shiv.js')}}"></script>
    <script src="{{asset('startuply/js/respond.min.js')}}"></script>
    <![endif]-->
</head>

<body id="landing-page" class="landing-page">

@yield('content')

<!--[if lt IE 9]>
<script type="text/javascript" src="{{asset('startuply/js/jquery-1.11.3.min.js?ver=1')}}"></script>
<![endif]-->
<!--[if (gte IE 9) | (!IE)]><!-->
<script type="text/javascript" src="{{asset('startuply/js/jquery-2.1.4.min.js?ver=1')}}"></script>
<!--<![endif]-->

<script type="text/javascript" src="{{asset('startuply/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.flexslider-min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.appear.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.plugin.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.countdown.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/startuply.js')}}"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-74496858-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-74496858-2');
</script>
</body>
</html>
