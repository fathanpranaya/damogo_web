<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta name="google-site-verification" content="YyGEbWLqcOp8b1PSCa8GpszMBncbkO7ZCDyvm2GtJZ0"/>
    <title>Home</title>
    <meta name="description" content="Startups template">
    <meta name="keywords" content="Startups template">
    <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
    <link rel="shortcut icon" href="{{asset('startuply/img/favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{asset('startuply/img/apple-touch-icon.jpg')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('startuply/img/apple-touch-icon-72x72.jpg')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('startuply/img/apple-touch-icon-114x114.jpg')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('startuply/css/custom-animations.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('startuply/css/lib/font-awesome.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('startuply/css/style.css')}}"/>

    <!--[if lt IE 9]>
    <script src="{{asset('startuply/js/html5shiv.js')}}"></script>
    <script src="{{asset('startuply/js/respond.min.js')}}"></script>
    <![endif]-->
</head>

<body id="landing-page" class="landing-page">
<!-- Preloader -->
<div class="preloader-mask">
    <div class="preloader">
        <div class="spin base_clr_brd">
            <div class="clip left">
                <div class="circle"></div>
            </div>
            <div class="gap">
                <div class="circle"></div>
            </div>
            <div class="clip right">
                <div class="circle"></div>
            </div>
        </div>
    </div>
</div>

{{--HEADER MENU SECTION--}}
<header>
    <nav class="navigation navigation-header white-dropdown">
        <div class="container">
            <div class="navigation-brand">
                <div class="brand-logo">
                    <a href="index.html" class="logo"></a><a href="index.html" class="logo logo-alt"></a>
                </div>
            </div>
            <button class="navigation-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navigation-navbar collapsed">
                <ul class="navigation-bar navigation-bar-left">
                    <li><a href="#hero">Home</a></li>
                    <li><a href="#howitworks">How It Works</a></li>
                    <li><a href="#faq">FAQ</a></li>
                    <li><a href="#aboutus">About</a></li>
                    <li><a href="#contactus">Contact Us</a></li>
                    <li><a href="{{url('business')}}">I'm A Business</a></li>
                </ul>
                <ul class="navigation-bar navigation-bar-right">
                    <li><a href="{{url('/')}}">EN</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>

{{--HOME SECTION--}}
<div id="hero" class="static-header image-version window-height light-text hero-section">
    <div class="container">
        <div class="heading-block animated align-center" data-animation="fadeIn" data-duration="700"
             style="padding-top: 135px;">
            <h2 style="font-family: 'Anton';">맛있고 신선한 음식을 70% 할인된 가격으로 구매하면서 음식물 폐기물로 부터 지구를 보호하세요.</h2>
            <p style="margin-bottom: 65px;"> 마트, 편의점, 베이커리 그리고 커피숍에서는 당일 영업이 끝나면 그날 판매되지 않았다는 이유만으로 온전한 음식을 폐기하고 있어요.
                DamaGO는 신선한 채로 버려지는 음식에 대한 해답을 찾았습니다! DamaGO를 통해 주변 매장의 신선하고 맛있는 음식을 할인된 가격으로 저렴하게
                구매하면서 음식물 낭비도 줄여 우리 지 구를 안전하게 지키는일에 동참해주세요.
            </p>
            <h1 style="font-family: 'Anton'; margin-bottom: 65px; font-size: 110px;">곧 찾아옵니다!!</h1>
        </div>
    </div>
    <div class="container align-center">
        <div class="col-sm-12 col-lg-12 animated" data-animation="fadeInLeft" data-duration="500">
            <article>
                <p class="">이메일 주소를 등록하고 다양한 할인 정보를 받아보세요. 이메일 주소 등록하기:</p>
            </article>
        </div>
        <div class="col-sm-12 col-lg-12 animated" data-animation="fadeInRight" data-duration="500">
            <form class="form mailchimp-form subscribe-form" style="padding-top: 10px;"
                  action="<?=$_SERVER['PHP_SELF']; ?>" method="post">
                <div class="form-group form-inline">
                    <input size="15" type="text" class="form-control required" name="FULLNAME" placeholder="이름"/>
                    <input size="25" type="email" class="form-control required" name="EMAIL" placeholder="이메일"/>
                    <input type="submit" class="btn btn-outline" value="등록/가입하기"/>
                    <span class="response"></span>
                </div>
            </form>
        </div>
    </div>
</div>

{{--HOW IT WORKS SECTION--}}
<section id="howitworks" class="section about-section align-center dark-text">
    <div class="container">
        <div class="tab-content alt">
            <div class="section-header align-center">
                <h2><span class="highlight">이용 </span>방법: </h2>
                <h4 class="sub-header animated" data-duration="700" data-animation="zoomIn">
                    It's as easy as...
                </h4>
            </div>
            <div class="section-content row animated" data-duration="700" data-delay="200" data-animation="fadeInDown">
                <div class="col-sm-4">
                    <article class="align-center">
                        <i class="howitworks icon icon-multimedia-20 highlight"></i>
                        <p class="thin">가까운 매장의 할인된 음식을 찾아드려요. 어플리케이션을 다운로드 하고 할인된 상품에 대한 푸쉬알 람을 수신하거나 인근 매장의 할인 상품을
                            검색할수 있습니다.</p>
                    </article>
                </div>
                <div class="col-sm-4">
                    <article class="align-center">
                        <i class="howitworks icon icon-shopping-18 highlight"></i>
                        <p class="thin">마음에 드는 상품을 어플리케이션을 통해 바로 구입하고 할당된 시간동안 픽업하면 끝!</p>
                    </article>
                </div>
                <div class="col-sm-4">
                    <article class="align-center">
                        <i class="howitworks icon icon-realestate-living-03 highlight"></i>
                        <p class="thin">소비자는 신선하고 맛있는 음식을 70% 할인된 가격으로 저렴하게 구입할 수 있고, 매장은 매출 증가 에 대한 이윤을 얻을수 있으며 이것은 우리
                            모두가 음식물 쓰레기를 줄여 지구를 보호할 수 있는 ‘윈- 윈 시츄에이션’입니다.</p>
                    </article>
                </div>
            </div>
            <br/>
            <br/>
        </div>
    </div>
</section>

<hr class="no-margin"/>

<section id="faq" class="section process-section align-center dark-text">
    <div class="container">
        <div class="section-content row">
            <div class="section-header">
                <h2><span class="highlight">FAQ </span></h2>
            </div>

            <div class="col-sm-12 align-left animated" data-duration="500" data-animation="fadeInLeft">
                <article>
                    <h5 class="highlight"><b>미판매 않은 음식을 먹어도 안전한가요?</b>
                    </h5>
                    <p class="sub-title">
                        네, 그렇습니다. DamaGO에 판매되는 음식들은 레스토랑이나 마트에서 일반적으로 판매되는 음식이에요. 이 런 음식들은 당일에 판매되지 않았다는 이유만으로 하루의 영업이
                        끝나는 시점에 또는 판매기간이 끝나는 시점 에 신선하고 온전한채로 버려지고 있습니다. 이렇게 버려지는 엄청난 양의 음식물 쓰레기로 인해 우리
                        지구는 오늘도 심각하게 오염되고 있어요.
                    </p>
                </article>
                <br/>
            </div>
        </div>
    </div>
</section>

<hr class="no-margin"/>

<section id="aboutus" class="section process-section align-center dark-text">
    <div class="container">
        <div class="section-content row">
            <div class="section-header">
                <h2><span class="highlight">Damogo는 </span> 무엇입니까? </h2>
                <h4><span class="highlight">저희의 </span> 임무</h4>
            </div>

            <div class="col-sm-12 align-left animated" data-duration="500" data-animation="fadeInLeft">
                <article>
                    <p class="sub-title">
                        통계에 따르면 전세계 음식물의 1/3은 버려진다고 해요 세계 곳곳, 먹을것이 없어 굶어 죽어가는 많은 사람들 이 있는 가운데 이런 낭비는 절대 있어서는 안되죠.
                    </p>
                </article>
                <br/>
                <article>
                    <p class="sub-title">
                        DamoGO는 이러한 문제에 조금이나마 도움이 될 수 있는 어플리케이션입니다. DamoGO는 음식물 낭비를 줄일 수 있게 하죠. 매장의 미판매분의 음식은 당일에 판매되지
                        않았다는 이유만으로 영업이 끝나는 시점에 신 선하고 온전한채로 버려지게 됩니다. 소비자들은 어플리케이션을 통해 이런 음식들을 저렴하게 구입할 수
                        있 고, 이는 음식 폐기물을줄여 지구를 보호할 수 있습니다.
                    </p>
                </article>
                <br/>
                <article>
                    <p class="sub-title">
                        이것은 윈-윈 시츄에이션입니다. 소비자는 맛있는 음식을 저렴하게 구입할수 있어서 좋고, 매장은 그만큼의 추 가 매출 올리며, 음식을 버리는데 드는 비용마저 절약할 수 있어서
                        좋죠. 그렇지만 더 중요하게는, 음식물을 썩 힐때 나오는 온실가스를 줄여 우리 모두가 지구 환경을 보존하는일에 동참할 수 있게 되요.
                    </p>
                </article>
            </div>
        </div>
    </div>
</section>

<!-- <section id="team" class="section team-section align-center dark-text"> -->
<section id="contactus" class="section features-section align-center inverted">
    <div class="container">
        <h2><span class="highlight">CONTACT</span> US FOR MORE INFO</h2>
        <div class="row">
            <div class="col-lg-7 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
                <form class="form form-register" id="registration" method="post" action="signup.php">
                    <div class="form-group">
                        <label for="fullname" class="col-sm-4 col-xs-12 control-label">이름</label>
                        <div class="col-sm-8 col-xs-12">
                            <input type="text" class="form-control required" name="fullname" id="fullname"
                                   placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-4 col-xs-12 control-label">이메일</label>
                        <div class="col-sm-8 col-xs-12">
                            <input type="email" class="form-control required email" name="email" id="email"
                                   placeholder="user@damogo.com">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-4 col-xs-12 control-label">메세지</label>
                        <div class="col-sm-8 col-xs-12">
                            <textarea id="message" name="message" class="form-control message"
                                      placeholder=""></textarea>
                        </div>
                    </div>
                    <input type="submit" class="btn-solid btn-lg btn-block" value="등록/가입하기"/>
                </form>

                <p class="agree-text align-center">
                    By clicking you agree to our Terms of Service, Privacy Policy & Refund Policy.
                </p>
            </div>
        </div>
    </div>
</section>

<footer id="footer" class="footer light-text">
    <div class="container">
        <div class="footer-content row">
            <div class="col-sm-4 col-xs-12">
                <div class="logo-wrapper">
                    <img width="130" height="31" src="{{asset('startuply/img/logo-white.png')}}" alt="logo"/>
                </div>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco. Qui officia deserunt mollit anim id est
                    laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco. Nisi ut
                    aliquid ex ea commodi consequatur?</p>
                <p><strong>John Doeson, Founder</strong>.</p>
            </div>
            <div class="col-sm-5 social-wrap col-xs-12">
                <strong class="heading">Social Networks</strong>
                <ul class="list-inline socials">
                    <li><a href="#"><span class="icon icon-socialmedia-08"></span></a></li>
                    <li><a href="#"><span class="icon icon-socialmedia-09"></span></a></li>
                    <li><a href="#"><span class="icon icon-socialmedia-16"></span></a></li>
                    <li><a href="#"><span class="icon icon-socialmedia-04"></span></a></li>
                </ul>
                <ul class="list-inline socials">
                    <li><a href="#"><span class="icon icon-socialmedia-07"></span></a></li>
                    <li><a href="#"><span class="icon icon-socialmedia-16"></span></a></li>
                    <li><a href="#"><span class="icon icon-socialmedia-09"></span></a></li>
                    <li><a href="#"><span class="icon icon-socialmedia-08"></span></a></li>
                </ul>
            </div>
            <div class="col-sm-3 col-xs-12">
                <strong class="heading">Our Contacts</strong>
                <ul class="list-unstyled">
                    <li><span class="icon icon-chat-messages-14"></span><a
                                href="mailto:info@startup.ly">info@startup.ly</a></li>
                    <li><span class="icon icon-seo-icons-34"></span>2901 Marmora road, Glassgow, Seattle, WA 98122-1090
                    </li>
                    <li><span class="icon icon-seo-icons-17"></span>1 - 234-456-7980</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="copyright">startup.ly 2014. All rights reserved.</div>
</footer>

<div class="back-to-top"><i class="fa fa-angle-up fa-3x"></i></div>

<!--[if lt IE 9]>
<script type="text/javascript" src="{{asset('startuply/js/jquery-1.11.3.min.js?ver=1')}}"></script>
<![endif]-->
<!--[if (gte IE 9) | (!IE)]><!-->
<script type="text/javascript" src="{{asset('startuply/js/jquery-2.1.4.min.js?ver=1')}}"></script>
<!--<![endif]-->

<script type="text/javascript" src="{{asset('startuply/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.flexslider-min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.appear.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.plugin.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.countdown.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('startuply/js/startuply.js')}}"></script>
</body>
</html>
