@extends('layout')

@section('content')
    <!-- Preloader -->
    <div class="preloader-mask">
        <div class="preloader">
            <div class="spin base_clr_brd">
                <div class="clip left">
                    <div class="circle"></div>
                </div>
                <div class="gap">
                    <div class="circle"></div>
                </div>
                <div class="clip right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>

    {{--HEADER MENU SECTION--}}
    <header>
        <nav class="navigation navigation-header white-dropdown">
            <div class="container">
                <div class="navigation-brand">
                    <div class="brand-logo">
                        <a href="{{url('/')}}" class="logo"></a><a href="{{url('/')}}" class="logo logo-alt"></a>
                    </div>
                </div>
                <button class="navigation-toggle">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navigation-navbar collapsed">
                    <ul class="navigation-bar navigation-bar-left">
                        <li><a href="#hero">Home</a></li>
                        <li><a href="#howitworks">How It Works</a></li>
                        <li><a href="#faq">FAQ</a></li>
                        <li><a href="#aboutus">About</a></li>
                        <li><a href="#contactus">Contact Us</a></li>
                        <li><a href="{{url('business')}}">I'm A Business</a></li>
                    </ul>
                    <ul class="navigation-bar navigation-bar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                {{ Config::get('languages')[App::getLocale()] }}
                            </a>
                            <ul class="dropdown-menu">
                                @foreach (Config::get('languages') as $lang => $language)
                                    @if ($lang != App::getLocale())
                                        <li>
                                            <a href="{{ route('lang.switch', $lang) }}">{{$language}}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    {{--HOME SECTION--}}
    <div id="hero" class="static-header image-version window-height light-text hero-section">
        <div class="container">
            <div class="heading-block animated align-center" data-animation="fadeIn" data-duration="700"
                 style="padding-top: 150px;">
                <h2 style="font-size: 60px">
                    {{trans('welcome.title_1')}}
                    <br>
                    {{trans('welcome.title_2')}}
                </h2>
                <h1 style="margin-bottom: 60px; margin-top: 60px; font-size: 140px;">
                    {{trans('welcome.coming_soon')}}
                </h1>
                <img style="margin-bottom: 50px" width="180" src="{{asset('startuply/img/favicon.png')}}" alt="logo"/>
            </div>
        </div>
        <div class="container align-center">
            <div class="col-sm-12 col-lg-12 animated" data-animation="fadeInLeft" data-duration="500">
                <article>
                    <p class="">{{trans('welcome.submit_email')}}</p>
                </article>
            </div>
            <div class="col-sm-12 col-lg-12 animated" data-animation="fadeInRight" data-duration="500">
                <form style="padding-top: 10px;" action="{{url('/subscribe')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group form-inline">
                        <input size="15" type="text" class="form-control required" name="name"
                               placeholder="{{trans('welcome.name')}}"/>
                        <input size="25" type="email" class="form-control required" name="email"
                               placeholder="{{trans('welcome.email')}}"/>
                        <input type="submit" class="btn btn-outline" value="{{trans('welcome.subscribe')}}"/>
                        <span class="response"></span>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--HOW IT WORKS SECTION--}}
    <section id="howitworks" class="section about-section align-center dark-text">
        <div class="container">
            <div class="tab-content alt">
                <div class="section-header align-center">
                    <h2><span class="highlight">{{trans('welcome.how')}} </span>{{trans('welcome.it_works')}} </h2>
                    <h4 class="sub-header animated" data-duration="700" data-animation="zoomIn">
                        {{trans('welcome.easy_as')}}
                    </h4>
                </div>
                <div class="section-content row animated" data-duration="700" data-delay="200" data-animation="fadeInDown">
                    <div class="col-sm-4">
                        <article class="align-center">
                            <img style="margin-bottom: 50px" width="200" src="{{asset('startuply/img/discover.png')}}" alt="logo"/>
                            <span class="heading">{{trans('welcome.discover')}}</span>
                            <p class="thin">{{trans('welcome.discover_desc')}}</p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article class="align-center">
                            <img style="margin-bottom: 50px" width="200" src="{{asset('startuply/img/purchase.png')}}" alt="logo"/>
                            <span class="heading">PURCHASE</span>
                            <p class="thin">
                                {{trans('welcome.purchase_desc_1')}}
                                <br>
                                {{trans('welcome.purchase_desc_2')}}
                                <br>
                                {{trans('welcome.purchase_desc_3')}}
                            </p>
                        </article>
                    </div>
                    <div class="col-sm-4">
                        <article class="align-center">
                            <img style="margin-bottom: 50px" width="200" src="{{asset('startuply/img/win.png')}}" alt="logo"/>
                            <span class="heading">{{trans('welcome.win')}}</span>
                            <p class="thin">
                                {{trans('welcome.win_desc')}}
                            </p>
                        </article>
                    </div>
                </div>
                <br/>
                <br/>
            </div>
        </div>
    </section>

    <hr class="no-margin"/>

    <section id="faq" class="section process-section align-center dark-text">
        <div class="container">
            <div class="section-content row">
                <div class="section-header">
                    <h2>
                        <i class="howitworks icon icon-office-50 highlight"></i>
                        <br>
                        <span>
                        {{trans('welcome.faq')}}
                    </span>
                    </h2>
                </div>

                <div class="col-sm-12 align-left animated" data-duration="500" data-animation="fadeInLeft">
                    <article>
                        <h5 class="highlight">
                            <b>{{trans('welcome.q_2')}}</b>
                        </h5>
                        <p class="sub-title">
                            {{trans('welcome.a_2')}}
                        </p>
                        <h5 class="highlight">
                            <b>{{trans('welcome.q_1')}}</b>
                        </h5>
                        <p class="sub-title">
                            {{trans('welcome.a_1')}}
                        </p>
                    </article>
                    <br/>
                </div>
            </div>
        </div>
    </section>

    <hr class="no-margin"/>

    <section id="aboutus" class="section process-section align-center dark-text">
        <div class="container">
            <div class="section-content row">
                <div class="section-header">
                    <h2>
                        <span class="highlight">{{trans('welcome.about')}} </span> {{trans('welcome.us')}}
                    </h2>
                    <h4>
                        <span class="highlight">{{trans('welcome.our')}} </span> {{trans('welcome.mission')}}
                    </h4>
                </div>

                <div class="col-sm-12 align-left animated" data-duration="500" data-animation="fadeInLeft">
                    <article>
                        <p class="sub-title">
                            {{trans('welcome.mission_1')}}
                        </p>
                    </article>
                    <br/>
                    <article>
                        <p class="sub-title">
                            {{trans('welcome.mission_2')}}
                        </p>
                    </article>
                    <br/>
                    <article>
                        <p class="sub-title">
                            {{trans('welcome.mission_3')}}
                        </p>
                    </article>
                </div>
            </div>
        </div>
    </section>

    <!-- <section id="team" class="section team-section align-center dark-text"> -->
    <section id="contactus" class="section features-section align-center inverted">
        <div class="container">
            <h2><span class="highlight">{{trans('welcome.contact')}}</span> {{trans('welcome.more_info')}}</h2>
            <div class="row">
                <div class="col-lg-7 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
                    <form class="form form-register" id="registration" method="post" action="{{url('/send')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="fullname" class="col-sm-4 col-xs-12 control-label">{{trans('welcome.name')}}</label>
                            <div class="col-sm-8 col-xs-12">
                                <input type="text" class="form-control required" name="name" id="name"
                                       placeholder="John Doe">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-4 col-xs-12 control-label">{{trans('welcome.email')}}</label>
                            <div class="col-sm-8 col-xs-12">
                                <input type="email" class="form-control required email" name="email" id="email"
                                       placeholder="user@damogo.com">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-4 col-xs-12 control-label">{{trans('welcome.message')}}</label>
                            <div class="col-sm-8 col-xs-12">
                            <textarea id="message" name="message" class="form-control message"
                                      placeholder="Enter your message here"></textarea>
                            </div>
                        </div>
                        <input type="submit" class="btn-solid btn-lg btn-block" value="{{trans('welcome.submit')}}"/>
                    </form>

                    <p class="agree-text align-center">
                        {{trans('welcome.privacy_policy')}}
                    </p>
                </div>
            </div>
        </div>
    </section>

    {{--<footer id="footer" class="footer light-text">--}}
    {{--<div class="container">--}}
    {{--<div class="footer-content row">--}}
    {{--<div class="col-sm-4 col-xs-12">--}}
    {{--<div class="logo-wrapper">--}}
    {{--<img width="130" height="31" src="{{asset('startuply/img/logo-white.png')}}" alt="logo"/>--}}
    {{--</div>--}}
    {{--<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco. Qui officia deserunt mollit anim id est--}}
    {{--laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco. Nisi ut--}}
    {{--aliquid ex ea commodi consequatur?</p>--}}
    {{--<p><strong>John Doeson, Founder</strong>.</p>--}}
    {{--</div>--}}
    {{--<div class="col-sm-5 social-wrap col-xs-12">--}}
    {{--<strong class="heading">Social Networks</strong>--}}
    {{--<ul class="list-inline socials">--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-08"></span></a></li>--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-09"></span></a></li>--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-16"></span></a></li>--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-04"></span></a></li>--}}
    {{--</ul>--}}
    {{--<ul class="list-inline socials">--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-07"></span></a></li>--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-16"></span></a></li>--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-09"></span></a></li>--}}
    {{--<li><a href="#"><span class="icon icon-socialmedia-08"></span></a></li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--<div class="col-sm-3 col-xs-12">--}}
    {{--<strong class="heading">Our Contacts</strong>--}}
    {{--<ul class="list-unstyled">--}}
    {{--<li><span class="icon icon-chat-messages-14"></span><a--}}
    {{--href="mailto:info@startup.ly">info@startup.ly</a></li>--}}
    {{--<li><span class="icon icon-seo-icons-34"></span>2901 Marmora road, Glassgow, Seattle, WA 98122-1090--}}
    {{--</li>--}}
    {{--<li><span class="icon icon-seo-icons-17"></span>1 - 234-456-7980</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="copyright">startup.ly 2014. All rights reserved.</div>--}}
    {{--</footer>--}}

    <div class="back-to-top"><i class="fa fa-angle-up fa-3x"></i></div>
@endsection
