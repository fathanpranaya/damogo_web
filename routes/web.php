<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('welcome');
});

Route::get('business', function () {
  return view('business');
});

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

Route::post('/send', 'EmailController@send');
Route::post('/sendbusiness', 'EmailController@sendBusiness');
Route::post('/subscribe', 'SubscriberController@store');
Route::get('/linfarassubs', 'SubscriberController@list');

